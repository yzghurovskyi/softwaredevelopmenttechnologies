package com.task1;

import java.util.Objects;

public class SmartNumber {
    private int number;

    public SmartNumber(int number) {
        this.number = number;
    }

    public int countDigits(){
        int digitsCounter = 0;
        int currNumber = number;

        do{
            digitsCounter++;
            currNumber /= 10;
        }
        while(currNumber != 0);

        return digitsCounter;
    }

    @Override
    public String toString() {
        return "SmartNumber{" +
                "number=" + number +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SmartNumber that = (SmartNumber) o;
        return number == that.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
