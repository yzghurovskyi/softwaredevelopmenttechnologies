package com.task2.main;

import com.task2.reader.ConsoleReader;
import com.task2.IntStorage;

public class Main {
    public static void main(String[] args){
        try{
            ConsoleReader consoleReader = new ConsoleReader();
            int arraySize = consoleReader.askArraySize();
            int[] results = consoleReader.readIntArray(arraySize);

            IntStorage helper = new IntStorage(results);
            System.out.println("Greatest common divisor: " + helper.greatestCommonDivisor());
            System.out.println("Least common multiple: " + helper.leastCommonMultiple());
        } catch(Exception ex){
            System.out.println(ex.toString());
        }

    }
}
