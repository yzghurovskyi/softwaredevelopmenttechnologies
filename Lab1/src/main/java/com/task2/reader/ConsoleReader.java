package com.task2.reader;

import java.util.Objects;
import java.util.Scanner;

public class ConsoleReader {
    private Scanner scanner;

    public ConsoleReader(){
        this.scanner = new Scanner(System.in);
    }

    public int askArraySize() {
        System.out.println("How many numbers do you need?");
        int result = 0;
        while(result == 0){
            try {
                System.out.print("Enter positive integer: ");
                String res = scanner.nextLine();
                result = Integer.parseInt(res);
            } catch (NumberFormatException err){
                System.out.println("NumberFormatException: " + err.getMessage());
            }
        }
        return result;
    }

    public int[] readIntArray(int arraySize) throws IllegalArgumentException{
        if(arraySize <= 0){
            throw new IllegalArgumentException("Array size must be positive integer");
        }
        int inputNumbers[] = new int[arraySize];

        System.out.println("You must enter " + arraySize + " numbers!");

        try {
            System.out.println("Enter an integer number and press <Enter>:");
            for (int i = 0; i < arraySize; i++) {
                inputNumbers[i] = scanner.nextInt();
            }

        } catch (NumberFormatException ex) {
            System.out.println("CAN NOT CONVERT STRING TO INTEGER FORMAT: " + ex);
        }
        System.out.println("Numbers successfully read");
        return inputNumbers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsoleReader that = (ConsoleReader) o;
        return Objects.equals(scanner, that.scanner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scanner);
    }


}
