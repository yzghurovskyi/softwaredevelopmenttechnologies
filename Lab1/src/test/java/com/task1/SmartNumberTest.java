package com.task1;

import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.CsvFileSource;
import static org.junit.jupiter.api.Assertions.*;

public class SmartNumberTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/smartNumberTestArguments.csv")
    void testSmartNumber(int number, int digitsCount){
        SmartNumber smartNumber = new SmartNumber(number);
        assertEquals(smartNumber.countDigits(), digitsCount);
    }

}
