package com.task2;

import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class IntStorageTest {

    private static Stream<Arguments> createArgumentsForIntArrayHelper() {
        return Stream.of(
                Arguments.of(new int[]{3, 4, 2, 5}, 1, 60),
                Arguments.of(new int[]{3}, 3, 3),
                Arguments.of(new int[]{}, 0, 1),
                Arguments.of(new int[]{54, 24, 8}, 2, 216));
    }

    @ParameterizedTest
    @MethodSource("createArgumentsForIntArrayHelper")
    void testIntArrayHelper(int[] numbers, int gcd, int lcm){
        IntStorage helper = new IntStorage(numbers);
        assertEquals(helper.greatestCommonDivisor(), gcd);
        assertEquals(helper.leastCommonMultiple(), lcm);
    }
}
