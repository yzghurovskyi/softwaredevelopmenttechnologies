package com.gmail.kp61group4.controller;

import com.gmail.kp61group4.model.Model;
import com.gmail.kp61group4.view.View;

public class ViewController {
    private View view;
    private Model model;

    public ViewController (View view, Model model) {
        this.view = view;
        this.model = model;
        addButtonListener();

    }
    public void addButtonListener () throws IllegalArgumentException {
        view.getButton().addActionListener((actionEvent) ->
        {
            try {
                model.setText(view.getTextField().getText());
            } catch (Exception e) {
                view.getTextArea().setText(e.toString());
            }
        });
    }

}
