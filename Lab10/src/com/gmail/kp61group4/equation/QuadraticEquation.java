package com.gmail.kp61group4.equation;

import com.gmail.kp61group4.exception.QuadraticEquationException;

public class QuadraticEquation {
    public double[] findRoots(double a, double b, double c) throws Exception {
        double[] roots = new double[2];
        double determinant = Math.pow(b, 2) - 4 * a * c;

        if(determinant > 0){
            double fraction = Math.sqrt(determinant) / (2 * a);
            if (a == 0) {
                System.out.println(a);
                throw new QuadraticEquationException("Param [a] can`t be 0");
            }
            roots[0] = -b + fraction;
            roots[1] = -b - fraction;
        } else if (determinant == 0){
            if (a == 0) {
                System.out.println(a);
                throw new QuadraticEquationException("Param [a] can`t be 0");
            }
            roots[0] = -b / (2 * a);
        } else {
            throw new Exception("Can not find real roots");
        }
        return roots;
    }
}
