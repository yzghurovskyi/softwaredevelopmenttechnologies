package com.gmail.kp61group4.exception;

public class QuadraticEquationException extends Exception {
    public QuadraticEquationException(String s) { super(s); }
}
