package com.gmail.kp61group4.model;


import com.gmail.kp61group4.equation.QuadraticEquation;
import com.gmail.kp61group4.exception.QuadraticEquationException;

import java.util.Observable;

public class Model extends Observable {
    private QuadraticEquation quadraticEquation;
    private double[] equationParams = new double[3];

    public Model(){
        quadraticEquation = new QuadraticEquation();
    }

    public void setText(String params) throws IllegalArgumentException {
            System.out.println(params);
            String[] paramsStringArr = params.split(",");
            if (paramsStringArr.length != 3) {
                throw new IllegalArgumentException("Arguments length must be 3");
            }
            double[] paramsArray = new double[3];
            int i = 0;
            for (String param : paramsStringArr) {
                if (!param.matches("[-+]?[0-9]*\\.?[0-9]?")) {
                    throw new NumberFormatException("Invalid number");
                }
                paramsArray[i] = Double.parseDouble(param);
                i++;
            }
            System.arraycopy(paramsArray, 0, equationParams, 0, 3);
            setChanged();
            notifyObservers();

    }

    public String getResults() throws QuadraticEquationException{
        String result = "Results: ";
        try {
            double[] roots = quadraticEquation.findRoots(equationParams[0], equationParams[1], equationParams[2]);
            for (double root : roots) {
                result += Double.toString(root) + " ,";
            }

        } catch (QuadraticEquationException ex) {
            result = ex.toString();
        } catch (Exception e) {
            result = e.toString();
        }
        return result;
    }

}
