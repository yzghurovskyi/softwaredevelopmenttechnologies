package com.gmail.kp61group4.view;

import com.gmail.kp61group4.exception.QuadraticEquationException;
import com.gmail.kp61group4.model.Model;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class View extends JFrame implements Observer {
    private JButton button;
    private JTextField textField;
    private JTextArea textArea;
    private JMenuItem menuOpen;
    private JMenuItem menuSave;
    JTextField textToSave;
//    private Text text = new Text();
    private Model model;

    public View (String title, Model model) {
        super(title);
        this.model = model;
        this.model.addObserver(this);
        setSize( 400, 500 );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        setLayout(new BorderLayout());       // set the layout manager
        textField = new JTextField("", 50);


        textToSave = new JTextField();
        Label label1 = new Label("");
        textArea = new JTextArea("");
        label1.setBackground(Color.WHITE);


        JPanel north = new JPanel();
        north.setLayout(new GridLayout(3, 2));
        north.add(label1);

        JLabel label = new JLabel("Enter a,b,c: ");
        JPanel textFieldPanel = new JPanel(new BorderLayout());
        textFieldPanel.add(label, BorderLayout.WEST);
        textFieldPanel.add(textField, BorderLayout.CENTER);
        north.add(textFieldPanel, BorderLayout.NORTH);
        add(north, BorderLayout.PAGE_START);


        button = new JButton("Calculate roots");

        textArea.setEditable(false);
        add(textArea);
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel, BorderLayout.PAGE_END);


    }

    public JButton getButton() {
        return button;
    }

    public JTextArea getTextArea() {
        return textArea;
    }

    public JTextField getTextField() {
        return textField;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (textArea != null) {
            textField.setText("");
            try {
                textArea.setText(model.getResults());
            } catch (QuadraticEquationException e) {
                textArea.setText(e.toString());
            }
        }
    }
}
