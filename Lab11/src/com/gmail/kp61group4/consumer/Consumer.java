package com.gmail.kp61group4.consumer;

import com.gmail.kp61group4.queue.Queue;

public class Consumer implements Runnable {
    private Queue queue;
    private int result;

    public Consumer(Queue queue){
        this.queue = queue;
        new Thread(this, "Consumer").start();
    }

    public void run() {
        while(true) {
            int nextNumber = queue.get();
            result = nextNumber * nextNumber - result * result;
        }
    }

    public double getResult(){
        return result;
    }
}
