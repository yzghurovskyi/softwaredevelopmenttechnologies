package com.gmail.kp61group4.main;

import com.gmail.kp61group4.consumer.Consumer;
import com.gmail.kp61group4.producer.Producer;
import com.gmail.kp61group4.queue.Queue;
import com.gmail.kp61group4.randomizer.Randomizer;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Queue queue = new Queue();
        Randomizer randomizer = new Randomizer(-100, 100);

        new Producer(queue, randomizer);

        Consumer consumer = new Consumer(queue);
        System.out.println("Для останова нажмите Ctrl-C");

        while (true){
            Thread.sleep(2000);
            System.out.println(consumer.getResult());
        }
    }
}


