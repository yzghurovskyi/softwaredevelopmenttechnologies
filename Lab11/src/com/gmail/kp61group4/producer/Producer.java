package com.gmail.kp61group4.producer;

import com.gmail.kp61group4.queue.Queue;
import com.gmail.kp61group4.randomizer.Randomizer;

public class Producer implements Runnable {
    private Queue queue;
    private Randomizer randomizer;

    public Producer(Queue queue, Randomizer randomizer){
        this.queue = queue;
        this.randomizer = randomizer;
        new Thread(this, "Producer").start();
    }
    public void run() {
        while(true){
            queue.put(randomizer.getRandomNumberInRange());
        }
    }
}
