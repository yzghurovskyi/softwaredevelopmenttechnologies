package com.gmail.kp61group4.queue;

public class Queue {
    private int number;
    private boolean valueSet = false;
    // синхронизированный метод
    public synchronized int get(){
        while(!valueSet)
            try {
                // сообщает потоку что нужно уступить монитор и
                // перейти в режим ожидания, пока другой поток не
                // захватит тот же монитор и не вызовет notify();
                wait();
            } catch (InterruptedException e){
                //возникло исключение
                System.out.println("InterruptedException выброшено");
            }
        //System.out.println("Получено: " + number);
        valueSet = false;
        //пробудить поток который вызвал wait(); на том
        // же самом объекте
        notify();
        return number;
    }
    // синхронизированный метод
    // получает данные из очереди
    public synchronized void put(int n){
        while (valueSet)
            try {
                wait();
            } catch(InterruptedException e){
                // Исключение
                System.out.println("InterruptedException выброшено");
            }
        this.number = n;
        valueSet = true;
        //System.out.println("Отдано: " + n);
        // пробуждаем поток
        notify();
    }
}
