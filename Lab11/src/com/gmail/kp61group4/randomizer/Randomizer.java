package com.gmail.kp61group4.randomizer;

public class Randomizer {
    private int min;
    private int max;

    public Randomizer(int min, int max){
        this.max = max;
        this.min = min;
    }

    public int getRandomNumberInRange() {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}