package com.gmail.kp61group4.task1.consoleReader;
import java.util.Scanner;


public class ConsoleReader {
    private Scanner scanner;

    public ConsoleReader(){
        this.scanner = new Scanner(System.in);
    }

    /**
     *
     * @return number of strings to read from console
     * @throws NumberFormatException
     */
    public int askArraySize() {
        System.out.println("How many numbers do you need?");
        int result = 0;
        while(result == 0){
            try {
                System.out.print("Enter positive integer: ");
                String res = scanner.nextLine();
                result = Integer.parseInt(res);
            } catch (NumberFormatException err){
                System.out.println("NumberFormatException: " + err.getMessage());
            }
        }
        return result;
    }

    /**
     * @param arraySize - number of string to read from console
     * @return strings from console input
     * @throws IllegalArgumentException
     */
    public String[] readStringArray(int arraySize) throws IllegalArgumentException{
        if(arraySize <= 0){
            throw new IllegalArgumentException("Array size must be positive integer");
        }
        String inputStrings[] = new String[arraySize];

        System.out.println("You must enter " + arraySize + " strings!");

        try {
            System.out.println("Enter string and press <Enter>:");

            for (int i = 0; i < arraySize; i++) {
                inputStrings[i] = scanner.nextLine();
            }

        } catch (NumberFormatException ex) {
            throw ex;
        }
        System.out.println("Strings successfully read");
        return inputStrings;
    }
}