package com.gmail.kp61group4.task1.developer;

import java.util.Objects;

public class Developer {
    private String name;
    private String surname;
    private int age;
    private String sex;

    public  Developer(){
        name = "";
        surname = "";
        age = -1;
        sex = "";
    }

    /**
     * Constructor
     * @param name - developer name
     * @param surname - developer surname
     */
    public  Developer(String name, String surname){
        this(name, surname,0);
    }

    /**
     * Constructor
     * @param name - developer name
     * @param surname - developer surname
     * @param age - developer age
     */
    public  Developer(String name,String surname, int age){
        this(name, surname, age, "");
    }

    /**
     * Constructor
     * @param name - developer name
     * @param surname - developer surname
     * @param age - developer age
     * @param sex - developer sex
     */
    public  Developer(String name,String surname, int age, String sex){
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Developer)) return false;
        Developer developer = (Developer) o;
        return age == developer.age &&
                Objects.equals(name, developer.name) &&
                Objects.equals(surname, developer.surname) &&
                Objects.equals(sex, developer.sex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, age, sex);
    }

    @Override
    public String toString() {
        return "Developer{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}
