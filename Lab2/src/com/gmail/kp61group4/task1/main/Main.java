package com.gmail.kp61group4.task1.main;

import com.gmail.kp61group4.task1.consoleReader.ConsoleReader;
import com.gmail.kp61group4.task1.developer.Developer;
import com.gmail.kp61group4.task1.stringProcess.StringProcessor;
import com.gmail.kp61group4.task1.stringProcess.StringStorage;
import com.gmail.kp61group4.task1.task.Task;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        ConsoleReader consoleReader = new ConsoleReader();
        String[] results;
        try{
            int arraySize = consoleReader.askArraySize();
            results = consoleReader.readStringArray(arraySize);
        } catch (Exception ex){
            System.out.println(ex.toString());
            return;
        }


        StringStorage stringStorage = new StringStorage(results);

        StringProcessor stringProcessor = new StringProcessor(stringStorage);
        stringProcessor.sortByLength();

        stringStorage = stringProcessor.getStringsToPrint();

        for (String s: stringStorage.getStrings()) {
            System.out.println(s);
        }
        System.out.println("--------------");

        Date taskStart = new Date(2018 - 1900,8,17);
        Task task = new Task.TaskBuilder("Ввести n строк с консоли. Упорядочить строки и вывести эти строки в порядке убывания длины, а также\n" +
                "значения их длин.")
                .startDate(taskStart)
                .endDate(new Date())
                .build();
        task.printInfo();
        System.out.println("--------------");
        System.out.println("Developers");
        Developer[] developers = new Developer[] {
                new Developer("Vadim", "Scherbina"),
                new Developer("Yaroslav", "Zghurovskyi"),
                new Developer("Ilya", "Telefus")
        };
        for(Developer dev: developers) System.out.println(dev);

    }
}
