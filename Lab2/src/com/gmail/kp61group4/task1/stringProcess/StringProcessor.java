package com.gmail.kp61group4.task1.stringProcess;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

public class StringProcessor {
    private StringStorage stringStorage;

    /**
     * @param stringStorage - contains strings for processing
     * @throws IllegalArgumentException
     */
    public StringProcessor(StringStorage stringStorage) throws IllegalArgumentException{
        if(stringStorage == null) throw new IllegalArgumentException("StringArray cannot be null");
        this.stringStorage = stringStorage;
    }

    /**
     * Sort strings by length
     */
    public void sortByLength(){
        Comparator<String> comparator = (s1,s2) -> s2.length() - s1.length();
        String[] strToSort = stringStorage.getStrings();
        Arrays.sort(strToSort, comparator);
        stringStorage.setStrings(strToSort);
    }

    /**
     * Return strings to print
     * @return storage of strings
     */
    public StringStorage getStringsToPrint(){
        for (int i = 0; i < stringStorage.length(); i++) {
            String str = stringStorage.getItemByIndex(i);
            stringStorage.setItemByIndex(i, str +"; length: " + str.length());
        }
        return stringStorage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stringStorage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringProcessor that = (StringProcessor) o;
        return Objects.equals(stringStorage, that.stringStorage);
    }

}
