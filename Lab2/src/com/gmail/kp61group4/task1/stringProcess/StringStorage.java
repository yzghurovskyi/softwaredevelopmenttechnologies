package com.gmail.kp61group4.task1.stringProcess;

import java.util.Arrays;

public class StringStorage {
    private String[] strings;

    /**
     * Constructor with storage size
     * @param size - storage size
     */
    public StringStorage(int size){
        strings = new String[size];
    }

    /**
     * Constructor
     * @param strings
     * @throws IllegalArgumentException
     */
    public StringStorage(String[] strings) throws IllegalArgumentException{
        if(strings == null) throw new IllegalArgumentException("Strings cannot be null");
        this.strings = Arrays.copyOf(strings, strings.length);
    }

    /**
     * Get copy of the string storage
     * @return strings
     */
    public String[] getStrings() {
        return Arrays.copyOf(strings, strings.length);
    }

    /**
     * setter
     * @param strings
     */
    public void setStrings(String[] strings) {
        this.strings = Arrays.copyOf(strings, strings.length);
    }

    /**
     * insert string to storage by index
     * @param index
     * @param str
     */
    public void setItemByIndex(int index, String str){
        strings[index] = str;
    }

    /**
     * get string from storage by index
     * @param index
     * @return
     */
    public String getItemByIndex(int index){
        return strings[index];
    }

    /**
     * get array length
     * @return length
     */
    public int length(){
       return strings.length;
    }

    @Override
    public String toString() {
        return "StringArray{" +
                "strings=" + Arrays.toString(strings) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringStorage that = (StringStorage) o;
        return Arrays.equals(strings, that.strings);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(strings);
    }
}
