package com.gmail.kp61group4.task1.task;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Task {
    private Date startDate;
    private Date endDate;
    private String description;

    public static class TaskBuilder {
        private Date startDate;
        private Date endDate;
        private String description;

        /**
         * Constructor
         * @param description
         */
        public TaskBuilder(String description) {
            this.description = description;
        }

        public TaskBuilder startDate(Date startDate) {
            this.startDate = startDate;
            return this;
        }

        public TaskBuilder endDate(Date endDate) {
            this.endDate = endDate;
            return this;
        }

        public Task build() {
            return new Task(this);
        }
    }

    private Task(TaskBuilder builder) {
        startDate = builder.startDate;
        endDate = builder.endDate;
        description = builder.description;
    }

    /**
     * print info about task
     */
    public void printInfo(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        System.out.println("task description: " + description);
        System.out.println("task start: " + dateFormat.format(startDate));
        System.out.println("task end: " + dateFormat.format(endDate));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(startDate, task.startDate) &&
                Objects.equals(endDate, task.endDate) &&
                Objects.equals(description, task.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDate, endDate, description);
    }
}
