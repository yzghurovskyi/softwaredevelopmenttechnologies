package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.storage.SmartDoubleStorage;
import com.gmail.kp61group4.task2.sum_visitors.BetweenNegativeSumVisitor;
import com.gmail.kp61group4.task2.sum_visitors.OddElementsSumVisitor;
import com.gmail.kp61group4.task2.transform_visitors.StorageCompressor;

public class Main {
    public static void main(String[] args){
        SmartDoubleStorage storage = new SmartDoubleStorage(new double[]{4.56, .11, -.98, 6.12, 4.67, -12.4, 0.34});
        System.out.println(storage);

        System.out.println("Sum between first and last negative numbers: "
                + storage.calculateSum(new BetweenNegativeSumVisitor()));

        System.out.println("Sum of odd elements: "
                + storage.calculateSum(new OddElementsSumVisitor()));

        storage.compress(new StorageCompressor());

        System.out.println(storage);
    }
}

