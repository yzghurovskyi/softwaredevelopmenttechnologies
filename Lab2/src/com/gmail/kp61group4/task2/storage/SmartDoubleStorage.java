package com.gmail.kp61group4.task2.storage;

import com.gmail.kp61group4.task2.sum_visitors.StorageSumVisitor;
import com.gmail.kp61group4.task2.transform_visitors.StorageTransformationVisitor;

import java.util.Arrays;


public class SmartDoubleStorage {
    private double[] numbers;

    public SmartDoubleStorage(double[] numbers) {
        if(numbers == null){
            throw new IllegalArgumentException("Argument <numbers> can not be null!");
        }
        this.numbers = Arrays.copyOf(numbers, numbers.length);
    }

    public void compress(StorageTransformationVisitor visitor){
        visitor.transformStorage(this);
    }

    public double calculateSum(StorageSumVisitor visitor){
        return visitor.calculateStorageSum(this);
    }

    public double getElementByIndex(int index){
        try {
            return numbers[index];
        } catch(IndexOutOfBoundsException ex) {
            System.out.println("Invalid index!!!\r\nTry again!");
            return Double.NaN;
        }
    }

    public int getStorageLength(){
        return numbers.length;
    }

    public void setValueByIndex(double value, int index){
        try {
            numbers[index] = value;
        } catch(IndexOutOfBoundsException ex){
            System.out.println("Invalid index!!!\r\nTry again!");
        }
    }

    @Override
    public String toString() {
        return "SmartDoubleStorage{" +
                "numbers=" + Arrays.toString(numbers) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SmartDoubleStorage that = (SmartDoubleStorage) o;
        return Arrays.equals(numbers, that.numbers);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(numbers);
    }
}
