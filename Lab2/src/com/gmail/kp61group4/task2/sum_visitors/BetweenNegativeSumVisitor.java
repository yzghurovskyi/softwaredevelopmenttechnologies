package com.gmail.kp61group4.task2.sum_visitors;

import com.gmail.kp61group4.task2.storage.SmartDoubleStorage;

public class BetweenNegativeSumVisitor implements StorageSumVisitor {
    @Override
    public double calculateStorageSum(SmartDoubleStorage storage) {
        int firstIndex = firstNegativeNumberIndex(storage);
        int lastIndex = lastNegativeNumberIndex(storage);
        double sum = 0;
        for (int i = firstIndex + 1; i <= lastIndex - 1; i++) {
            sum += storage.getElementByIndex(i);
        }
        return sum;
    }

    private int firstNegativeNumberIndex(SmartDoubleStorage storage){
        int firstIndex = 0;
        int length = storage.getStorageLength();
        while(firstIndex < length && storage.getElementByIndex(firstIndex) >= 0){
            firstIndex++;
        }
        if(firstIndex == length) firstIndex = -1;
        return firstIndex;
    }

    private int lastNegativeNumberIndex(SmartDoubleStorage storage){
        int lastIndex = storage.getStorageLength() - 1;
        while(lastIndex >= 0 && storage.getElementByIndex(lastIndex) >= 0){
            lastIndex--;
        }
        return lastIndex;
    }
}
