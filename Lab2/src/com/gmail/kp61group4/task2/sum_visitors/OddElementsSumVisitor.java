package com.gmail.kp61group4.task2.sum_visitors;


import com.gmail.kp61group4.task2.storage.SmartDoubleStorage;

public class OddElementsSumVisitor implements StorageSumVisitor {
    @Override
    public double calculateStorageSum(SmartDoubleStorage storage) {
        double sum = 0;
        int length = storage.getStorageLength();
        for(int i = 1; i < length; i += 2){
            sum += storage.getElementByIndex(i);
        }
        return sum;
    }
}
