package com.gmail.kp61group4.task2.sum_visitors;

import com.gmail.kp61group4.task2.storage.SmartDoubleStorage;

public interface StorageSumVisitor {
    double calculateStorageSum(SmartDoubleStorage storage);
}
