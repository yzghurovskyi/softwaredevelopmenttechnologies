package com.gmail.kp61group4.task2.transform_visitors;

import com.gmail.kp61group4.task2.storage.SmartDoubleStorage;

public class StorageCompressor implements StorageTransformationVisitor {
    @Override
    public void transformStorage(SmartDoubleStorage storage) {
        int length = storage.getStorageLength();
        int lastNonZeroIndex = length - 1;
        for(int i = 0; i <= lastNonZeroIndex; i++){
            double element = storage.getElementByIndex(i);
            if(Math.abs(element) <= 1){
                storage.setValueByIndex(0 ,i);
                swap(storage, i, lastNonZeroIndex);
                lastNonZeroIndex--;
                i--;
            }
        }
    }

    private void swap(SmartDoubleStorage storage, int index1, int index2){
        double temp = storage.getElementByIndex(index1);
        storage.setValueByIndex(storage.getElementByIndex(index2), index1);
        storage.setValueByIndex(temp, index2);
    }
}
