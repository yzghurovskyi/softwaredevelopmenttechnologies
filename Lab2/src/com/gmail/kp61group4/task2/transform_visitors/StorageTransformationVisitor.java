package com.gmail.kp61group4.task2.transform_visitors;

import com.gmail.kp61group4.task2.storage.SmartDoubleStorage;

public interface StorageTransformationVisitor {
    void transformStorage(SmartDoubleStorage storage);
}
