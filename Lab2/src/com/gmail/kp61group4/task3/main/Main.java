package com.gmail.kp61group4.task3.main;

import com.gmail.kp61group4.task3.stringProcessing.StringBuilder;

public class Main {
    public static void main(String[] args){
        StringBuilder stringBuilder = new StringBuilder("OLegeg");
        stringBuilder.append("qweqwe ").append("io  werwe12rwer 234 34 sdf1qwe").append("  werwer");
        System.out.println(stringBuilder);

        stringBuilder.replaceWords(2, "badum");
        stringBuilder.replaceWords(3, null);
        System.out.println(stringBuilder);
    }
}
