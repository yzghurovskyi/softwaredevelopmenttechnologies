package com.gmail.kp61group4.task3.stringProcessing;

import java.util.Objects;

public class StringBuilder {
    private String stringContent;

    public StringBuilder(){
        stringContent = "";
    }

    public StringBuilder(String startString){
        stringContent = startString;
    }

    public StringBuilder append(String toAppend) {
        stringContent += toAppend;
        return this;
    }

    public StringBuilder replaceWords(int wordLength, String newWord){
        if(newWord == null) newWord = "";
        stringContent = stringContent.replaceAll(String.format("\\b[\\'a-zA-Z0-9]{%s}\\b", wordLength), newWord);
        return this;
    }

    public String getValue(){return stringContent;}


    public boolean equals(StringBuilder o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringBuilder that = (StringBuilder) o;
        return Objects.equals(stringContent, that.stringContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stringContent);
    }

    @Override
    public String toString(){
        return stringContent;
    }
}
