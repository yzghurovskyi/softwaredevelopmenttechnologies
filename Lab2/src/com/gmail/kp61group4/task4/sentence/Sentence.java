package com.gmail.kp61group4.task4.sentence;

import java.util.Objects;

public class Sentence {
    private String sentence;
    private int vowelCount;
    private int consonantCount;

    public Sentence() {
        sentence = "";
        vowelCount = 0;
        consonantCount = 0;
    }

    public Sentence(String sentence) {
        this.sentence = sentence;
        vowelCount = 0;
        consonantCount = 0;
    }

    public void append(String str) {
        sentence += str;
    }

    public boolean isMoreVowel() {
        for (char c : sentence.toCharArray()) {
            if (Character.isLetter(c)) {
                if (isVowel(c)) vowelCount++;
                else consonantCount++;
            }
        }
        return vowelCount > consonantCount;
    }

    private boolean isVowel(char c) {
        return "AEIOUaeiou".indexOf(c) != -1;
    }

    @Override
    public String toString() {
        return "Sentence='" + sentence + '\'' +
                ", vovelCount=" + vowelCount +
                ", consonantCount=" + consonantCount;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence1 = (Sentence) o;
        return vowelCount == sentence1.vowelCount &&
                consonantCount == sentence1.consonantCount &&
                Objects.equals(sentence, sentence1.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence, vowelCount, consonantCount);
    }
}
