package com.gmail.kp61group4.task1.abiturient;

import com.gmail.kp61group4.task1.address.Address;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Abiturient {
    private int id;
    private String surname;
    private String name;
    private String fatherName;
    private Address adress;
    private String phoneNumber;
    private List<Integer> marks;

    public Abiturient(int size){
        id = -1;
        surname = "";
        name = "";
        fatherName = "";
        phoneNumber = "";
        marks = new ArrayList<Integer>();
    }

    public Abiturient(int id, String surname, String name, String fatherName, Address adress, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
        this.marks = new ArrayList<Integer>();
    }

    public static class AbiturientBuilder {
        private int id;
        private String surname;
        private String name;
        private String fatherName;
        private Address adress;
        private String phoneNumber;

        public AbiturientBuilder(int id) {
            this.id = id;
        }

        public AbiturientBuilder surname(String surname){
            this.surname = surname;
            return this;
        }

        public AbiturientBuilder name(String name){
            this.name = name;
            return this;
        }

        public AbiturientBuilder fatherName(String fatherName){
            this.fatherName = fatherName;
            return this;
        }

        public AbiturientBuilder adress(Address adress){
            this.adress = adress;
            return this;
        }

        public AbiturientBuilder phoneNumber(String phoneNumber){
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Abiturient build(){
            return new Abiturient(this);
        }
    }

    private Abiturient(AbiturientBuilder builder){
        this.id = builder.id;
        this.name = builder.name;
        this.surname = builder.surname;
        this.fatherName = builder.fatherName;
        this.adress = builder.adress;
        this.phoneNumber = builder.phoneNumber;
        this.marks = new ArrayList<Integer>();
    }

    public Abiturient addMark(int mark) throws IllegalArgumentException {
        if(mark > 0) {
            marks.add(mark);
        } else {
            throw new IllegalArgumentException();
        }
        return this;
    }

    public double calculateAverageMark() {
        double sum = 0.0;
        for(int mark: marks) {
            sum += mark;
        }
        return sum / marks.size();
    }

    public boolean hasBadMarks() {
        for(int mark: marks) {
           if(mark < 3) {
               return true;
           }
        }
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public Address getAdress() {
        return adress;
    }

    public void setAdress(Address adress) {
        this.adress = adress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Integer> getMarks() {
        return marks;
    }

    public void setMarks(List<Integer> marks) {
        this.marks = marks;
    }

    @Override
    public String toString() {
        return "Abiturient{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", adress='" + adress + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Abiturient that = (Abiturient) o;
        return id == that.id &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(name, that.name) &&
                Objects.equals(fatherName, that.fatherName) &&
                Objects.equals(adress, that.adress) &&
                Objects.equals(phoneNumber, that.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, surname, name, fatherName, adress);
    }
}
