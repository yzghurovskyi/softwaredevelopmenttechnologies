package com.gmail.kp61group4.task1.abiturientStorage;

import com.gmail.kp61group4.task1.abiturient.Abiturient;

import java.util.*;

public class AbiturientStorage {
    private List<Abiturient> abiturients;

    public AbiturientStorage() {
        abiturients = new ArrayList<>();
    }

    public void add(Abiturient abiturient) {
        abiturients.add(abiturient);
    }

    public List<Abiturient> getAbiturientsWithBadMarks() {
        List<Abiturient> abiturientsWithBadMarks = new ArrayList<>();
        for(Abiturient a: abiturients){
            if(a.hasBadMarks()){
                abiturientsWithBadMarks.add(a);
            }
        }
        return abiturientsWithBadMarks;
    }

    public List<Abiturient> getAbiturientsWithHigherAverageRating(double rating) {
        List<Abiturient> abiturientsWithAverageRating = new ArrayList<>();
        for (Abiturient a : abiturients) {
            if (a.calculateAverageMark() > rating) {
                abiturientsWithAverageRating.add(a);
            }
        }
        return abiturientsWithAverageRating;
    }

    public List<Abiturient> getAbiturientsWithHighRating(int abitCount) throws IllegalArgumentException {
        if(abitCount <= abiturients.size()) {
            Comparator<Abiturient> comparator = (a1, a2) -> (int) (a1.calculateAverageMark() - a2.calculateAverageMark());
            abiturients.sort(comparator);
            return abiturients.subList(0, abitCount);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public Abiturient removeAbiturientByIndex(int index) {
        return abiturients.remove(index);
    }

    public void setAbiturientByIndex(int index, Abiturient abiturient) {
        abiturients.set(index,abiturient);
    }

    public Abiturient getAbiturientByIndex(int index) {
        return abiturients.get(index);
    }

    public List<Abiturient> getAbiturients() {
        return new ArrayList<>(abiturients);
    }

    public void setAbiturients(List<Abiturient> abiturients) {
        this.abiturients = new ArrayList<>(abiturients);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbiturientStorage that = (AbiturientStorage) o;
        return Objects.equals(abiturients, that.abiturients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(abiturients);
    }
}
