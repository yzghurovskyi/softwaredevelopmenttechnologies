package com.gmail.kp61group4.task1.main;

import com.gmail.kp61group4.task1.abiturient.Abiturient;
import com.gmail.kp61group4.task1.abiturientStorage.AbiturientStorage;
import com.gmail.kp61group4.task1.address.Address;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Address ad1 = new Address("Uman", "Green", 10,5);
        Abiturient a1 = new Abiturient.AbiturientBuilder(1).surname("Pupkin").name("Vasya")
                .fatherName("Volodimirovich").adress(ad1).phoneNumber("3880965478562").build();
        Address ad2 = new Address("Kiev", "Yellow", 10,5);
        Abiturient a2 = new Abiturient.AbiturientBuilder(2).surname("Savchenko").name("Ivan")
                .fatherName("Victorovich").adress(ad2).phoneNumber("3880965478562").build();
        Address ad3 = new Address("Odesa", "White", 10,5);
        Abiturient a3 = new Abiturient.AbiturientBuilder(3).surname("Gavrilyk").name("Uhim")
                .fatherName("Volodimirovich").adress(ad3).phoneNumber("3880965478562").build();
        a1.addMark(5).addMark(1).addMark(4).addMark(2);
        a2.addMark(5).addMark(4).addMark(4).addMark(3);
        a3.addMark(5).addMark(4).addMark(4).addMark(4);
        AbiturientStorage abitStorage  = new AbiturientStorage();
        abitStorage.add(a1);
        abitStorage.add(a2);
        abitStorage.add(a3);
        List<Abiturient> abiturientsWithBadMarks = abitStorage.getAbiturientsWithBadMarks();
        List<Abiturient> abiturientsWithAverageRating = abitStorage.getAbiturientsWithHigherAverageRating(4.2);
        List<Abiturient> abiturientsWithHighRating = abitStorage.getAbiturientsWithHighRating(2);

        System.out.println("-----------------------------");
        System.out.println("Abiturients with bad marks:");
        for(Abiturient a: abiturientsWithBadMarks){
            System.out.println(a);
        }
        System.out.println("-----------------------------");
        System.out.println("Abiturients with average rating:");
        for(Abiturient a: abiturientsWithAverageRating){
            System.out.println(a);
        }
        System.out.println("-----------------------------");

        System.out.println("Abiturients with highest rating:");
        for(Abiturient a: abiturientsWithHighRating){
            System.out.println(a);
        }
        System.out.println("-----------------------------");

    }
}
