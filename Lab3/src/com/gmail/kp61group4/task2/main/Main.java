package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.matrix.Matrix;
import com.gmail.kp61group4.task2.matrixStorage.MatrixStorage;

public class Main {
    public static void main(String[] args){
        var data = new double[][]{
                {1, 2, -1},
                {2, 10, 3},
                {-1, -7, 3}
        };
        Matrix m = new Matrix(data);
        Matrix m1 = new Matrix(3);
        m1.fillByRandom(1, 7);
        Matrix m2 = new Matrix(3);
        m2.fillByRandom(1, 7);

        MatrixStorage storage = new MatrixStorage(3);
        storage.add(m);
        storage.add(m1);
        storage.add(m2);
        for(var matrix : storage.getMatrices()){
            System.out.println(matrix);
        }
        int minFirstNormIndex = storage.findWithMinFirstNorm();
        int minSecondNormIndex = storage.findWithMinSecondNorm();

        System.out.println("Matrix with min first norm:\r\n" + storage.getMatrixByIndex(minFirstNormIndex));
        System.out.println("Matrix with min second norm:\r\n" + storage.getMatrixByIndex(minSecondNormIndex));

        System.out.println("ADD MATRICES\n");
        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m1.add(m2));

        System.out.println("MINUS MATRICES\n");
        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m1.minus(m2));

        System.out.println("MULTIPLY MATRICES\n");
        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m1.multiply(m2));

    }
}
