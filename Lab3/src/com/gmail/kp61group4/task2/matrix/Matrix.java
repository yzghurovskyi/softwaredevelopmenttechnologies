package com.gmail.kp61group4.task2.matrix;

import java.util.Arrays;

public class Matrix {
    private double[][] data;

    public Matrix(int dimension){
        if(dimension <= 0) throw new IllegalArgumentException("Dimension must be positive number!");
        else {
            this.data = new double[dimension][dimension];
        }
    }

    public Matrix(double[][] data) {
        this(data.length);
        int dimension = data.length;
        if(!isCorrectDimension(data))
            throw new IllegalArgumentException("All rows and cols must have the same length.");

        for (int i = 0; i < dimension; i++)
            System.arraycopy(data[i], 0, this.data[i], 0, dimension);
    }

    public void fillByRandom(double min, double max){
        int dimension = getDimension();
        for(int i = 0; i < dimension; i++){
            for(int j = 0; j < dimension; j++) data[i][j] = Math.random() * (max - min) + min;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return matrixDataEquals(data, matrix.data);
    }

    private boolean matrixDataEquals(double[][] data1, double[][] data2){
        for (int i = 0; i < data1.length; i++){
            if(!Arrays.equals(data1[i], data2[i])) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder matrixString = new StringBuilder("----------" +
                "\nMatrix\n");
        int dimension = getDimension();
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                matrixString.append(String.format("%-8.2f", data[i][j]));
            }
            matrixString.append('\n');
        }
        return matrixString.append("----------").toString();
    }

    public Matrix add(Matrix matrix){
        if(isCorrectDimensions(this, matrix))
            throw new IllegalArgumentException("Matrix doesn't have correct dimension");
        int dimension = getDimension();
        Matrix result = new Matrix(dimension);
        for(int i = 0; i < dimension; i++) {
            for(int j = 0; j < dimension; j++) {
                result.data[i][j] = data[i][j] + matrix.data[i][j];
            }
        }
        return result;
    }

    public Matrix minus(Matrix matrix){
        if(isCorrectDimensions(this, matrix))
            throw new IllegalArgumentException("Matrix doesn't have correct dimension");
        int dimension = getDimension();
        Matrix result = new Matrix(dimension);
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                result.data[i][j] = data[i][j] - matrix.data[i][j];
            }
        }
        return result;
    }

    public Matrix multiply(Matrix matrix){
        if(isCorrectDimensions(this, matrix))
            throw new IllegalArgumentException("Matrix is not square matrix");
        int dimension = getDimension();
        Matrix result = new Matrix(dimension);
        for(int i = 0; i < dimension; i++) {
            for(int j = 0; j < dimension; j++) {
                for(int k = 0; k < dimension; k++) {
                    result.data[i][j] += data[i][k] * matrix.data[k][j];
                }
            }
        }
        return result;
    }

    private int getDimension(){
        return data.length;
    }

    public double firstNorm(){
        int dimension = getDimension();
        double norm = -1;
        for(int i = 0; i < dimension; i++){
            double sum = 0d;
            for(int j = 0; j < dimension; j++){
                sum += Math.abs(data[i][j]);
            }
            norm = Math.max(norm, sum);
        }
        return norm;
    }

    public double secondNorm(){
        int dimension = getDimension();
        double norm = -1;
        for(int j = 0; j < dimension; j++){
            double sum = 0d;
            for(int i = 0; i < dimension; i++){
                sum += Math.abs(data[i][j]);
            }
            norm = Math.max(norm, sum);
        }
        return norm;
    }

    public static boolean isCorrectDimension(double[][] data){
        int dimension = data[0].length;
        int rows = 0;
        for (double[] row : data){
            rows++;
            if (row.length != dimension) return false;
        }
        return rows == dimension;
    }

    private static boolean isCorrectDimensions(Matrix m1, Matrix m2) {
        return m1.getDimension() != m2.getDimension();
    }
}
