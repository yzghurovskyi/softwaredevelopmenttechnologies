package com.gmail.kp61group4.task2.matrixStorage;

import com.gmail.kp61group4.task2.matrix.Matrix;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MatrixStorage {
    private List<Matrix> matrices;

    public MatrixStorage(int size){
        matrices = new ArrayList<>(size);
    }

    public MatrixStorage(){
        matrices = new ArrayList<>();
    }

    public MatrixStorage(List<Matrix> matrices){
        this.matrices = new ArrayList<>(matrices);
    }

    public void add(Matrix matrix){
        matrices.add(matrix);
    }

    public int findWithMinFirstNorm(){
        int minIndex = 0;
        for(int i = 1; i < matrices.size(); i++){
            if(matrices.get(i).firstNorm() < matrices.get(minIndex).firstNorm()){
                minIndex = i;
            }
        }
        return minIndex;
    }

    public int findWithMinSecondNorm(){
        int minIndex = 0;
        for(int i = 1; i < matrices.size(); i++){
            if(matrices.get(i).secondNorm() < matrices.get(minIndex).secondNorm()){
                minIndex = i;
            }
        }
        return minIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatrixStorage that = (MatrixStorage) o;
        return Objects.equals(matrices, that.matrices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(matrices);
    }


    public Matrix removeMatrixByIndex(int index) {
        return matrices.remove(index);
    }

    public void setMatrixByIndex(int index, Matrix matrix) {
        matrices.set(index, matrix);
    }

    public Matrix getMatrixByIndex(int index) {
        return matrices.get(index);
    }

    public List<Matrix> getMatrices() {
        return new ArrayList<>(matrices);
    }
}
