package com.gmail.kp61group4.task3.complex;

public class Complex {
    private double re, im;

    public Complex(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public Complex(double re){
        this(re, 0.0);
    }

    public Complex(){
        this(0.0, 0.0);
    }

    public Complex(Complex z){
        this(z.re, z.im) ;
    }

    public double getRe(){return re;}
    public double getlmf(){return im;}
    public Complex getZ(){return new Complex(re, im);}
    public void setRe(double re){this.re = re;}
    public void setlm(double im){this.im = im;}
    public void setZ(Complex z){re = z.re; im = z.im;}

    public void add(Complex z){re += z.re; im += z.im;}
    public void sub(Complex z){re -= z.re; im -= z.im;}

    public void mul(Complex z){
        double t = re * z.re - im * z.im;
        im = re * z.im + im * z.re;
        re = t;
    }


    public Complex plus(Complex z){
        return new Complex(re + z.re, im + z.im);
    }

    public Complex minus(Complex z){
        return new Complex(re - z.re, im - z.im);
    }

    public Complex multiply(Complex z){
        return new Complex(
                re * z.re - im * z.im, re * z.im + im * z.re);

    }


    @Override
    public String toString() {
        return "Complex{" +
                "re=" + re +
                ", im=" + im +
                '}';
    }
}
