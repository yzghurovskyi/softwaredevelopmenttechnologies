package com.gmail.kp61group4.task3.complexVector;

import com.gmail.kp61group4.task3.complex.Complex;

import java.util.ArrayList;
import java.util.List;

public class ComplexVector {
    private List<Complex> vector;

    public ComplexVector(){
        this.vector = new ArrayList<>();
    }
    public ComplexVector(List<Complex> vector){
        this.vector = vector;
    }

    public void add(Complex complex) {
        vector.add(complex);
    }

    public ComplexVector add(ComplexVector complexVector) {
        if(vector.size() != complexVector.vector.size()){
            throw new IllegalArgumentException("Lengths must be equals");
        }
        List<Complex> addedVector = new ArrayList<>();
        for(int i = 0; i < vector.size(); i++){
            Complex addedNumbers = vector.get(i).plus(complexVector.vector.get(i));
            addedVector.add(addedNumbers);
        }

        return new ComplexVector(addedVector);
    }

    public ComplexVector multiply(ComplexVector complexVector) {
        if(vector.size() != complexVector.vector.size()){
            throw new IllegalArgumentException("Lengths must be equals");
        }
        List<Complex> multiplyVector = new ArrayList<>();
        for(int i = 0; i < vector.size(); i++){
            Complex addedNumbers = vector.get(i).multiply(complexVector.vector.get(i));
            multiplyVector.add(addedNumbers);
        }
        return new ComplexVector(multiplyVector);
    }

    public void print(){
        for(Complex number: vector){
            System.out.println(number);
        }
    }
}
