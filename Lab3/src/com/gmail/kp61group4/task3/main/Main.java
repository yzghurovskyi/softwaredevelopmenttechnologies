package com.gmail.kp61group4.task3.main;

import com.gmail.kp61group4.task3.complex.Complex;
import com.gmail.kp61group4.task3.complexVector.ComplexVector;

public class Main {
    public static void main(String[] args){
        Complex num1 = new Complex(1,3);
        Complex num2 = new Complex(5,2);
        Complex num3 = new Complex(3,4);
        Complex num4 = new Complex(1,1);
        ComplexVector complexVector = new ComplexVector();
        complexVector.add(num1);
        complexVector.add(num2);
        ComplexVector complexVector1 = new ComplexVector();
        complexVector1.add(num3);
        complexVector1.add(num4);
        ComplexVector addedNumbers = complexVector.add(complexVector1);

        addedNumbers.print();
        System.out.println("-----------------------------");
        ComplexVector myltiplyNumbers = complexVector.multiply(complexVector1);
        myltiplyNumbers.print();
    }
}
