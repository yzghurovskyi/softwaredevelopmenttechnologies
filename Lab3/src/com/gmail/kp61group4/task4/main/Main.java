package com.gmail.kp61group4.task4.main;

import com.gmail.kp61group4.task4.page.Page;
import com.gmail.kp61group4.task4.text.Text;
import com.gmail.kp61group4.task4.textColor.TextColor;
import com.gmail.kp61group4.task4.word.Word;

public class Main {
    public static void main(String[] args){
        Page page1 = new Page();
        page1.addWord(new Word("Vadim ")).addWord(new Word("likes ")).addWord(new Word("beer."));

        Page page2 = new Page();
        page2.addWord(new Word("Vadim ")).addWord(new Word("is ")).addWord(new Word("from ")).addWord(new Word("Uman."));

        Text text = new Text();
        text.addPage(page1).addPage(page2);

        text.print(TextColor.YELLOW);
    }
}
