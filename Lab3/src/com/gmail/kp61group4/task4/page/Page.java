package com.gmail.kp61group4.task4.page;

import com.gmail.kp61group4.task4.word.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Page {
    private List<Word> words;

    public Page(){
        words = new ArrayList<>();
    }

    public Page addWord(Word word){
        words.add(word);
        return this;
    }

    @Override
    public String toString(){
        StringBuilder pageText = new StringBuilder();
        for(Word word : words){
            pageText.append(word);
        }
        return pageText.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page page = (Page) o;
        return Objects.equals(words, page.words);
    }

    @Override
    public int hashCode() {
        return Objects.hash(words);
    }
}
