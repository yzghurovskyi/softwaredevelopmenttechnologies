package com.gmail.kp61group4.task4.text;

import com.gmail.kp61group4.task4.page.Page;
import com.gmail.kp61group4.task4.textColor.TextColor;
import com.gmail.kp61group4.task4.word.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Text {
    private List<Page> pages;

    public Text(){
        pages = new ArrayList<>();
    }

    public Text addPage(Page page){
        pages.add(page);
        return this;
    }

    public Text addWordToLastPage(Word word){
        int pagesCount = pages.size();
        pages.get(pagesCount - 1).addWord(word);
        return this;
    }

    public void print(TextColor textColor){
        System.out.println("-----------THIS TEXT COLOR: " + textColor.name() + " (" + textColor.getShortColorValue() + ")-------------");
        System.out.println(toString());
        System.out.println("------------END TEXT----------------");
    }

    @Override
    public String toString(){
        StringBuilder text = new StringBuilder();
        for(Page page : pages){
            text.append(page);
            text.append('\n');
        }
        return text.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text = (Text) o;
        return Objects.equals(pages, text.pages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pages);
    }
}
