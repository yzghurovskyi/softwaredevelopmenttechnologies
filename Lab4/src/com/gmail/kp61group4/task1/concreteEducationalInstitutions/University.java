package com.gmail.kp61group4.task1.concreteEducationalInstitutions;

import com.gmail.kp61group4.task1.educationalInstitution.EducationalInstitution;

public class University implements EducationalInstitution {
    private String name;
    private int studentsCount;

    public University(String name, int studentsCount){
        this.name = name;
        this.studentsCount = studentsCount;
    }

    @Override
    public String getName() {
        return "University: " + name;
    }

    @Override
    public int getStudentsCount() {
        return studentsCount;
    }

    @Override
    public void printInfo() {
        System.out.println("INFO: University name: " + name + "\r\nNumbers of students in university: " + studentsCount);
    }
}
