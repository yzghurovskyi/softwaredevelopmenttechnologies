package com.gmail.kp61group4.task1.educationalInstitution;

public interface EducationalInstitution {
    String getName();
    int getStudentsCount();
    void printInfo();
}
