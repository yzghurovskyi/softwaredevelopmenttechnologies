package com.gmail.kp61group4.task1.main;

import com.gmail.kp61group4.task1.concreteEducationalInstitutions.College;
import com.gmail.kp61group4.task1.concreteEducationalInstitutions.University;
import com.gmail.kp61group4.task1.educationalInstitution.EducationalInstitution;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<EducationalInstitution> institutions = new ArrayList<>() {{
            add(new College("MAUP", 84));
            add(new University("KNU", 23000));
            add(new University("KPI", 34000));
        }};

        for(var institution : institutions){
            institution.printInfo();
        }
    }
}
