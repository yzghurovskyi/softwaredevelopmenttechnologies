package com.gmail.kp61group4.task2.credit;

import com.gmail.kp61group4.task2.subject.Subject;

import java.util.Objects;

public class Credit {
    private Subject subject;
    private int score;

    public Credit(Subject subject, int score) {
        this.subject = subject;
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Credit)) return false;
        Credit credit = (Credit) o;
        return score == credit.score &&
                Objects.equals(subject, credit.subject);
    }

    @Override
    public int hashCode() {

        return Objects.hash(subject, score);
    }

    @Override
    public String toString() {
        return "Credit{" +
                "subject=" + subject +
                ", score=" + score +
                '}';
    }
}
