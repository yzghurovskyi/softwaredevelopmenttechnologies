package com.gmail.kp61group4.task2.exam;

import com.gmail.kp61group4.task2.subject.Subject;

import java.util.Objects;

public class Exam {
    private int max_score;
    private Subject subject;

    public Exam(int max_score, Subject subject) {
        this.max_score = max_score;
        this.subject = subject;
    }



    @Override
    public int hashCode() {

        return Objects.hash(max_score, subject);
    }

    @Override
    public String toString() {
        return "Exam{" +
                "max_score=" + max_score +
                ", subject=" + subject +
                '}';
    }
}
