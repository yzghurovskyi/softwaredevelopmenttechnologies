package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.credit.Credit;
import com.gmail.kp61group4.task2.exam.Exam;
import com.gmail.kp61group4.task2.recordBook.RecordBook;
import com.gmail.kp61group4.task2.session.Session;
import com.gmail.kp61group4.task2.subject.Subject;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Subject subject = new Subject("Phisics", 40, 60);
        Credit credit = new Credit(subject, 100);
        Exam exam = new Exam(40, subject);
        Session session = new Session(2018, 2, subject);
        List<Exam> exams = new ArrayList<>();
        exams.add(exam);
        List<Credit> credits = new ArrayList<>();
        credits.add(credit);
        List<Session> sessions = new ArrayList<>();
        sessions.add(session);
        RecordBook recordBook = new RecordBook("Dima", "Kovalchuk", "KP6310",exams, credits,sessions);
        recordBook.addExam(new Exam(4, subject));
        recordBook.addSession(new Session(2017, 5, subject));
        recordBook.addCredit(new Credit(subject, 5));
        recordBook.displayStudyInformatiom();
    }
}
