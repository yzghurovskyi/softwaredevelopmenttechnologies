package com.gmail.kp61group4.task2.recordBook;

import com.gmail.kp61group4.task2.credit.Credit;
import com.gmail.kp61group4.task2.exam.Exam;
import com.gmail.kp61group4.task2.session.Session;

import java.util.ArrayList;
import java.util.List;

public class RecordBook {

    private String name;
    private String surname;
    private String recordBookIndetificator;
    private Study study;

    public RecordBook(String name, String surname, String recordBookIndetificator, List<Exam> exams, List<Credit> credits, List<Session> sessions) {
        this.name = name;
        this.surname = surname;
        this.recordBookIndetificator = recordBookIndetificator;
        this.study = new Study(exams, credits, sessions);
    }

    public void addExam(Exam exam){
        study.exams.add(exam);
    }

    public void addSession(Session session){
        study.sessions.add(session);
    }

    public void addCredit(Credit credit){
        study.credits.add(credit);
    }

    private class Study {
        private List<Exam> exams;
        private List<Credit> credits;
        private List<Session> sessions;

        private Study(List<Exam> exams, List<Credit> credits, List<Session> sessions) {
            this.exams = new ArrayList<>(exams);
            this.credits = new ArrayList<>(credits);
            this.sessions = new ArrayList<>(sessions);
        }

        private void printInfoAboutExams(){
            for(Exam ex: exams){
                System.out.println(ex);
            }
        }

        private void printInfoAboutCredits(){
            for(Credit credit: credits){
                System.out.println(credit);
            }
        }

        private void printInfoAboutSessions(){
            for(Session session: sessions){
                System.out.println(session);
            }
        }
    }

    @Override
    public String toString() {
        return "RecordBook{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", recordBookIndetificator='" + recordBookIndetificator + '\'' +
                '}';
    }

    public void displayStudyInformatiom() {
        study.printInfoAboutCredits();
        study.printInfoAboutExams();
        study.printInfoAboutSessions();
    }
}
