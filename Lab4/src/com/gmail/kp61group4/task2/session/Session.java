package com.gmail.kp61group4.task2.session;

import com.gmail.kp61group4.task2.subject.Subject;

import java.util.Objects;

public class Session {
    private int year;
    private int examCount;
    private Subject subject;

    public Session(int year, int examCount, Subject subject) {
        this.year = year;
        this.examCount = examCount;
        this.subject = subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        Session session = (Session) o;
        return year == session.year &&
                examCount == session.examCount &&
                Objects.equals(subject, session.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, examCount, subject);
    }

    @Override
    public String toString() {
        return "Session{" +
                "year=" + year +
                ", examCount=" + examCount +
                ", subjects=" + subject +
                '}';
    }
}
