package com.gmail.kp61group4.task2.subject;

import java.util.Objects;

public class Subject {
    private String name;
    private int hours;
    private int maxScoreBeforeExam;

    public Subject(String name, int hours, int maxScoreBeforeExam) {
        this.name = name;
        this.hours = hours;
        this.maxScoreBeforeExam = maxScoreBeforeExam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subject)) return false;
        Subject subject = (Subject) o;
        return hours == subject.hours &&
                maxScoreBeforeExam == subject.maxScoreBeforeExam &&
                Objects.equals(name, subject.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, hours, maxScoreBeforeExam);
    }

    @Override
    public String toString() {
        return "Subject{" +
                "name='" + name + '\'' +
                ", hours=" + hours +
                ", maxScoreBeforeExam=" + maxScoreBeforeExam +
                '}';
    }
}
