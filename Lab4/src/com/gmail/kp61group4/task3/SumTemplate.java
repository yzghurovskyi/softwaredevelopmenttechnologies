package com.gmail.kp61group4.task3;

import com.gmail.kp61group4.task3.sum_strategies.StorageSumStrategy;

public abstract class SumTemplate implements StorageSumStrategy {
    public abstract double calculateStorageSum(double[] storage);

    public void show(){

    }
}
