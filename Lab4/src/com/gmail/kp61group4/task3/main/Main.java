package com.gmail.kp61group4.task3.main;

import com.gmail.kp61group4.task3.storage.SmartDoubleStorage;
import com.gmail.kp61group4.task3.sum_strategies.BetweenNegativeSumStrategy;
import com.gmail.kp61group4.task3.sum_strategies.OddElementsSumStrategy;

public class Main {
    public static void main(String[] args){
        SmartDoubleStorage storage = new SmartDoubleStorage(new double[]{4.56, .11, -.98, 6.12, 4.67, -12.4, 0.34});
        System.out.println(storage);

        storage.setSumStrategy(new BetweenNegativeSumStrategy());
        System.out.println("Sum between first and last negative numbers: "
                + storage.calculateSum());

        storage.setSumStrategy(new OddElementsSumStrategy());
        System.out.println("Sum of odd elements: "
                + storage.calculateSum());
    }
}
