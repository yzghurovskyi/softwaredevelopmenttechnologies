package com.gmail.kp61group4.task3.sum_strategies;

public class BetweenNegativeSumStrategy implements StorageSumStrategy {
    @Override
    public double calculateStorageSum(double[] storage) {
        int firstIndex = firstNegativeNumberIndex(storage);
        int lastIndex = lastNegativeNumberIndex(storage);
        double sum = 0;
        for (int i = firstIndex + 1; i <= lastIndex - 1; i++) {
            sum += storage[i];
        }
        return sum;
    }

    private int firstNegativeNumberIndex(double[] storage){
        int firstIndex = 0;
        int length = storage.length;
        while(firstIndex < length && storage[firstIndex] >= 0){
            firstIndex++;
        }
        if(firstIndex == length) firstIndex = -1;
        return firstIndex;
    }

    private int lastNegativeNumberIndex(double[] storage){
        int lastIndex = storage.length - 1;
        while(lastIndex >= 0 && storage[lastIndex] >= 0){
            lastIndex--;
        }
        return lastIndex;
    }
}
