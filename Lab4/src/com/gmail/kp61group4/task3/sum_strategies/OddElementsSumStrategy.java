package com.gmail.kp61group4.task3.sum_strategies;

import com.gmail.kp61group4.task3.SumTemplate;

public class OddElementsSumStrategy extends SumTemplate{
    @Override
    public double calculateStorageSum(double[] storage) {
        double sum = 0;
        int length = storage.length;
        for (int i = 1; i < length; i += 2) {
            sum += storage[i];
        }
        return sum;
    }
}
