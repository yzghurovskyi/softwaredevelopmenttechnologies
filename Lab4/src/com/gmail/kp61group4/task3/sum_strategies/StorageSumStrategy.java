package com.gmail.kp61group4.task3.sum_strategies;

public interface StorageSumStrategy {
    double calculateStorageSum(double[] storage);
}
