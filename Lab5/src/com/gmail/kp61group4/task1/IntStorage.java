package com.gmail.kp61group4.task1;

import java.util.Arrays;

public class IntStorage {
    private int[] numbers;

    public IntStorage(int[] numbers) throws IllegalArgumentException{
        if(numbers == null) throw new IllegalArgumentException("Numbers cannot be null");
        this.numbers = new int[numbers.length];
        System.arraycopy(numbers, 0, this.numbers, 0, numbers.length);
    }

    public int greatestCommonDivisor(){
        return Arrays.stream(numbers).reduce(0, this::greatestCommonDivisor);
    }

    public int leastCommonMultiple(){
        return Arrays.stream(numbers).reduce(1, this::leastCommonMultiple);
    }

    private int greatestCommonDivisor(int first, int second){
        return (second == 0) ? first : greatestCommonDivisor(second, first % second);
    }

    private int leastCommonMultiple(int first, int second){
        try{
            return first * second / greatestCommonDivisor(first, second);
        } catch (java.lang.ArithmeticException e) {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "IntStorage{" +
                "numbers=" + Arrays.toString(numbers) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntStorage that = (IntStorage) o;
        return Arrays.equals(numbers, that.numbers);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(numbers);
    }
}
