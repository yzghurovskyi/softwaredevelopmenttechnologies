package com.gmail.kp61group4.task1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {

    // JFrame
    private JFrame f;

    // JButton
    private JButton button;

    // label to display text
    private JLabel label, label2;

    // text area
    private JTextArea jt, jt1;

    public class NumberProcess implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String s = e.getActionCommand();
            if (s.equals("submit")) {
                int[] input = new int[2];
                try {
                    input[0] = Integer.parseInt(jt.getText());
                    input[1] = Integer.parseInt(jt1.getText());
                } catch (NumberFormatException ex) {
                    label.setText("Enter numbers only!");
                }

                IntStorage storage = new IntStorage(input);
                label.setText("Greatest common divisor is: " + storage.greatestCommonDivisor());
                label2.setText("Least common multiple is: " + storage.leastCommonMultiple());
            } else return;
        }
    }

    public Window() {
        f = new JFrame("Welcome");

        // create a label to display text
        label = new JLabel();
        label2 = new JLabel();

        // create a new buttons
        button = new JButton("submit");
        button.addActionListener(new NumberProcess());

        jt = new JTextArea("enter number ", 10, 20);
        jt1 = new JTextArea("enter number ", 10, 20);

        JPanel p = new JPanel();

        // add the text area and button to panel
        p.add(jt);
        p.add(jt1);
        p.add(button);
        p.add(label);
        p.add(label2);

        f.add(p);
        // set the size of frame
        f.setSize(600, 500);

        f.show();
    }
}
