package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.sentance.Sentence;
import com.gmail.kp61group4.task2.text.Text;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {
    private JButton button;
    private JTextField textField;
    private JTextArea textArea;
    private Text text = new Text();

    public Window (String title) {
        super(title);
        setSize( 400, 500 );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        setLayout(new BorderLayout());       // set the layout manager
        textField = new JTextField("", 50);

        JLabel label = new JLabel("Enter text: ");
        JPanel textFieldPanel = new JPanel(new BorderLayout());
        textFieldPanel.add(label, BorderLayout.WEST);
        textFieldPanel.add(textField, BorderLayout.CENTER);
        add(textFieldPanel, BorderLayout.PAGE_START);

        button = new JButton("Process text");

        textArea = new JTextArea("");
        textArea.setEditable(false);
        add(textArea);
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel, BorderLayout.PAGE_END);
        button.addActionListener((actionEvent) -> {
            Sentence sentence = new Sentence(textField.getText());
            text.add(sentence);
            text.textProcess();
            textField.setText("");
            textArea.setText(text.printText());
        });

    }
}
