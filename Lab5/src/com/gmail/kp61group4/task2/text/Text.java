package com.gmail.kp61group4.task2.text;

import com.gmail.kp61group4.task2.sentance.Sentence;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Text {
    private List<Sentence> text;

    public Text(String text) {
        this.text = textToStringList(text);
    }
    public Text() {
        this.text = new ArrayList<>();
    }

    private List<Sentence> textToStringList(String text) {
        ArrayList<Sentence> stringList = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getSentenceInstance();
        text = text.replaceAll("(\r\n|\r|\n)", "");
        iterator.setText(text);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            stringList.add(new Sentence(text.substring(start,end)));
        }
        return stringList;
    }

    public void textProcess(){
        for(Sentence sentence: text){
            if(!sentence.getSentence().contains("Vowels are more than consonants\n") &&
                    !sentence.getSentence().contains("Consonants are more than vowels\n")) {
                String result = (sentence.isMoreVowel()) ? " - Vowels are more than consonants\n" : " - Consonants are more than vowels\n";
                sentence.append(result);
            }
        }
    }

    public String printText() {
        String textToTextArea = "";
        for(Sentence sen : text) {
            textToTextArea += sen.getSentence();
        }
        return textToTextArea;
    }

    public void add(Sentence sentence) {
        text.add(sentence);
    }

    public Sentence removeItemByIndex(int index) {
        return text.remove(index);
    }

    public void setItemByIndex(int index, Sentence sentence) {
        text.set(index,sentence);
    }

    public Sentence getItemByIndex(int index) {
        return text.get(index);
    }

    public List<Sentence> getText() {
        return new ArrayList<>(text);
    }

    public void setText(List<Sentence> text) {
        this.text = new ArrayList<>(text);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text1 = (Text) o;
        return Objects.equals(text, text1.text);
    }
}
