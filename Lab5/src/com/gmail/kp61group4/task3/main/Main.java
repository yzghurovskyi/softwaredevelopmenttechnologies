package com.gmail.kp61group4.task3.main;


import com.gmail.kp61group4.task3.main.Window;
import com.gmail.kp61group4.task3.page.Page;
import com.gmail.kp61group4.task3.text.Text;
import com.gmail.kp61group4.task3.textColor.TextColor;
import com.gmail.kp61group4.task3.word.Word;

public class Main {
    public static void main(String[] args){
        Window frame = new Window("Hello"); // construct a MyFrame object
        frame.setVisible( true );
    }
}
