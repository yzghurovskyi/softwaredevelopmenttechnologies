package com.gmail.kp61group4.task3.main;

import com.gmail.kp61group4.task2.sentance.Sentence;
import com.gmail.kp61group4.task3.page.Page;
import com.gmail.kp61group4.task3.text.Text;
import com.gmail.kp61group4.task3.word.Word;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {
    private JButton buttonAddWord;
    private JButton buttonAddPage;
    private JTextField textField;
    private JTextArea textArea;
    private Text text = new Text();
    private JLabel label;
    private JPanel panel;

    public Window (String title) {
        super(title);
        setSize( 400, 500 );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        setLayout(new BorderLayout());       // set the layout manager
        textField = new JTextField("", 50);

        JLabel label = new JLabel("Enter text: ");
        JPanel textFieldPanel = new JPanel(new BorderLayout());
        textFieldPanel.add(label, BorderLayout.WEST);
        textFieldPanel.add(textField, BorderLayout.CENTER);
        add(textFieldPanel, BorderLayout.PAGE_START);

        buttonAddWord = new JButton("Add word");
        buttonAddPage = new JButton("Add page");

        textArea = new JTextArea("");
        textArea.setEditable(false);
        add(textArea);
        panel = new JPanel();
        panel.add(buttonAddWord);
        panel.add(buttonAddPage);
        add(panel, BorderLayout.PAGE_END);
        Page page = new Page();

        ActionListener addWordListener = actionEvent -> {
            Word word = new Word(textField.getText());
            page.addWord(word);
            textField.setText("");
            textArea.setText(page.getText());
        };

        ActionListener addPageListener = actionEvent -> {
            try {
                text.addPage(page);
                textArea.setText(text.getText());
                page.clearPage();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        };

        buttonAddWord.addActionListener(addWordListener);
        buttonAddPage.addActionListener(addPageListener);

    }
}
