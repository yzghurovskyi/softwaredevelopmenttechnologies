package com.gmail.kp61group4.task3.textColor;

public enum TextColor {
    BLACK(0){
        @Override
        public String getShortColorValue(){
            return "BL";
        }
    },
    RED(1) {
        @Override
        public String getShortColorValue() {
            return "RE";
        }
    },
    YELLOW(2) {
        @Override
        public String getShortColorValue() {
            return "YE";
        }
    },
    GREEN(3) {
        @Override
        public String getShortColorValue() {
            return "GR";
        }
    };

    private int colorCode;

    TextColor(int colorCode){
        this.colorCode = colorCode;
    }

    public abstract String getShortColorValue();

}