package com.gmail.kp61group4.task4.actionListeners;

import com.gmail.kp61group4.task4.concreteEducationalInstitutions.College;
import com.gmail.kp61group4.task4.concreteEducationalInstitutions.University;
import com.gmail.kp61group4.task4.educationalInstitution.EducationalInstitution;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InfoListener implements ActionListener {
    private JTextField name;
    private JSpinner count;
    private JTextArea infoArea;
    private ButtonGroup buttonGroup;

    public InfoListener(JTextField name, JSpinner count, JTextArea infoArea, ButtonGroup buttonGroup){
        this.name = name;
        this.count = count;
        this.infoArea = infoArea;
        this.buttonGroup = buttonGroup;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        EducationalInstitution institution;
        String insName = name.getText();
        int insCount = (int)count.getModel().getValue();
        switch (buttonGroup.getSelection().getActionCommand()){
            case "University":{
                institution = new University(insName, insCount);
                break;
            }
            case "College":{
                institution = new College(insName, insCount);
                break;
            }
            default:
                throw new IllegalArgumentException("Illegal selected institution");
        }
        infoArea.setText(" ");
        infoArea.append(institution.info() + "\n");
    }
}
