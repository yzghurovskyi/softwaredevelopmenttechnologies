package com.gmail.kp61group4.task4.concreteEducationalInstitutions;

import com.gmail.kp61group4.task4.educationalInstitution.EducationalInstitution;

import java.util.Objects;

public class College implements EducationalInstitution {
    private String name;
    private int studentsCount;

    public College(String name, int studentsCount){
        this.name = name;
        this.studentsCount = studentsCount;
    }

    @Override
    public String getName() {
        return "College: " + name;
    }

    @Override
    public int getStudentsCount() {
        return studentsCount;
    }

    @Override
    public String info() {
        return "INFO: College name: " + name + "\r\nNumbers of students in college: " + studentsCount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        College college = (College) o;
        return studentsCount == college.studentsCount &&
                Objects.equals(name, college.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, studentsCount);
    }

    @Override
    public String toString() {
        return "College{" +
                "name='" + name + '\'' +
                ", studentsCount=" + studentsCount +
                '}';
    }
}
