package com.gmail.kp61group4.task4.educationalFrame;

import com.gmail.kp61group4.task4.actionListeners.InfoListener;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class EducationalFrame extends JFrame {
    private JTextField name = new JTextField("Enter name:", 50);
    private SpinnerModel countModel = new SpinnerNumberModel(50, 0, 500000, 1);
    private JSpinner count = new JSpinner(countModel);

    private JRadioButton universityButton = new JRadioButton("University");
    private JRadioButton collegeButton = new JRadioButton("College");

    private JButton showButton = new JButton("Show info");
    private JTextArea infoArea = new JTextArea();


    public EducationalFrame(){
        super("Educational");
        this.setSize(600,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new GridLayout(3, 1));

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(universityButton);
        buttonPanel.add(collegeButton);

        ButtonGroup group = new ButtonGroup();
        group.add(universityButton);
        group.add(collegeButton);
        universityButton.setActionCommand("University");
        collegeButton.setActionCommand("College");
        universityButton.setSelected(true);

        JPanel dataPanel = new JPanel();
        dataPanel.setLayout(new FlowLayout());
        dataPanel.add(name);
        dataPanel.add(count);
        dataPanel.add(showButton);
        add(buttonPanel);
        add(dataPanel);
        add(infoArea);
        infoArea.setEditable(false);

        showButton.addActionListener(new InfoListener(name, count, infoArea, group));
    }
}
