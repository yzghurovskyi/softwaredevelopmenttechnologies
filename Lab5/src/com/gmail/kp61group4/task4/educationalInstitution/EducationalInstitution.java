package com.gmail.kp61group4.task4.educationalInstitution;

public interface EducationalInstitution {
    String getName();
    int getStudentsCount();
    String info();
}
