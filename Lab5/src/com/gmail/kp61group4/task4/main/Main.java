package com.gmail.kp61group4.task4.main;

import com.gmail.kp61group4.task4.educationalFrame.EducationalFrame;

public class Main {

    public static void main(String[] args) {
        EducationalFrame frame = new EducationalFrame();
        frame.setVisible(true);
    }
}
