package com.gmail.kp61group4.task1.controller;

import com.gmail.kp61group4.task1.model.Model;
import com.gmail.kp61group4.task1.view.ConsoleView;

public class ConsoleController {

    private ConsoleView view;
    private Model model;

    public ConsoleController (ConsoleView view, Model model) {
        this.view = view;
        this.model = model;
    }

    public void run () {
        while (true) {
            String text = view.getText();
            model.setText(text);
        }
    }
}
