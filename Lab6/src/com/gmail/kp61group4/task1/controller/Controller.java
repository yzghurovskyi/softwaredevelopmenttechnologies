package com.gmail.kp61group4.task1.controller;

import com.gmail.kp61group4.task1.model.Model;
import com.gmail.kp61group4.task1.view.View;

public class Controller {

    private View view;
    private Model model;

    public Controller (View view, Model model) {
        this.view = view;
        this.model = model;

        view.getButton().addActionListener((actionEvent) -> model.setText(view.getTextField().getText()));
    }
}