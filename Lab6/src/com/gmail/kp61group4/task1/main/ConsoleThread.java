package com.gmail.kp61group4.task1.main;

import com.gmail.kp61group4.task1.view.ConsoleView;
import com.gmail.kp61group4.task1.controller.ConsoleController;
import com.gmail.kp61group4.task1.model.Model;

public class ConsoleThread  implements Runnable {
    private Model model;

    public ConsoleThread(Model model) {
        this.model = model;
    }

    @Override
    public void run() {
        ConsoleView view = new ConsoleView( model);
        ConsoleController controller = new ConsoleController(view, model);
        controller.run();
    }
}
