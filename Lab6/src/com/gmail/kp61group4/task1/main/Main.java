package com.gmail.kp61group4.task1.main;
import com.gmail.kp61group4.task1.model.Model;

public class Main {
    public static void main(String[] args) {
        Model model = new Model();

        ConsoleThread consoleThread = new ConsoleThread(model);
        ViewThread viewThread = new ViewThread(model);
        Thread thread1 = new Thread(consoleThread);
        Thread thread2 = new Thread(viewThread);
        thread1.start();
        thread2.start();
    }
}
