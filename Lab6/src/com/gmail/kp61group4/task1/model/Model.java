package com.gmail.kp61group4.task1.model;

import com.gmail.kp61group4.task1.sentence.Sentence;
import com.gmail.kp61group4.task1.text.Text;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private Text text;
    private List<Observer> listeners;

    public Model(){
        text = new Text();
        listeners = new ArrayList();
    }

    public Text getText() {
        return text;
    }

    public void setText(String sentance) {
        Sentence sentence = new Sentence(sentance);
        text.add(sentence);
        text.textProcess();
        this.updateObservers();
    }

    void updateObservers () {
        for (Observer observer: listeners) {
            observer.update();
        }
    }

    public void register (Observer observer) {
        listeners.add(observer);
        observer.update();
    }
}
