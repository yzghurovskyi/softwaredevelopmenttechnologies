package com.gmail.kp61group4.task1.model;

public interface Observer {
    void update();
}
