package com.gmail.kp61group4.task1.view;

import com.gmail.kp61group4.task1.model.Model;
import com.gmail.kp61group4.task1.model.Observer;

import java.util.Scanner;

public class ConsoleView implements Observer {
    private Model model;
    private Scanner scanner;


    public ConsoleView(Model model) {
        this.model = model;
        this.model.register(this);
        this.scanner = new Scanner(System.in);
    }

    public String getText() {
        System.out.println("Enter string: ");
        String res = scanner.nextLine();
        return res;
    }

    @Override
    public void update() {
        System.out.println(model.getText().printText());

    }
}
