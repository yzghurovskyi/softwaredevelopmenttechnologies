package com.gmail.kp61group4.task1.view;

import com.gmail.kp61group4.task1.model.Model;
import com.gmail.kp61group4.task1.model.Observer;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame implements Observer{
    private JButton button;
    private JTextField textField;
    private JTextArea textArea;
    private Model model;

    public View (String title, Model model) {
        super(title);
        this.model = model;
        this.model.register(this);

        setSize( 400, 500 );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        setLayout(new BorderLayout());       // set the layout manager
        textField = new JTextField("", 50);

        JLabel label = new JLabel("Enter text: ");
        JPanel textFieldPanel = new JPanel(new BorderLayout());
        textFieldPanel.add(label, BorderLayout.WEST);
        textFieldPanel.add(textField, BorderLayout.CENTER);
        add(textFieldPanel, BorderLayout.PAGE_START);

        button = new JButton("Process text");

        textArea = new JTextArea("");
        textArea.setEditable(false);
        add(textArea);
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel, BorderLayout.PAGE_END);


    }

    public JButton getButton() {
        return button;
    }

    public JTextField getTextField() {
        return textField;
    }

    public JTextArea getTextArea() {
        return textArea;
    }



    @Override
    public void update() {
        if (textArea != null) {
            textField.setText("");
            textArea.setText(model.getText().printText());
        }
    }
}
