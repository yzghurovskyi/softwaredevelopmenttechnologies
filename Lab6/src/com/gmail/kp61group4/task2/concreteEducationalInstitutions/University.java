package com.gmail.kp61group4.task2.concreteEducationalInstitutions;

import com.gmail.kp61group4.task2.educationalInstitution.EducationalInstitution;

import java.util.Objects;

public class University implements EducationalInstitution {
    private String name;
    private int studentsCount;

    public University(String name, int studentsCount){
        this.name = name;
        this.studentsCount = studentsCount;
    }

    @Override
    public String getName() {
        return "University: " + name;
    }

    @Override
    public int getStudentsCount() {
        return studentsCount;
    }

    @Override
    public String info() {
        return "INFO: University name: " + name + "\r\nNumbers of students in university: " + studentsCount;
    }

    @Override
    public String toString() {
        return "University{" +
                "name='" + name + '\'' +
                ", studentsCount=" + studentsCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        University that = (University) o;
        return studentsCount == that.studentsCount &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, studentsCount);
    }
}
