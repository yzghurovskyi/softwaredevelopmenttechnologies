package com.gmail.kp61group4.task2.controller;

import com.gmail.kp61group4.task2.concreteEducationalInstitutions.College;
import com.gmail.kp61group4.task2.concreteEducationalInstitutions.University;
import com.gmail.kp61group4.task2.educationalInstitution.EducationalInstitution;
import com.gmail.kp61group4.task2.model.Model;
import com.gmail.kp61group4.task2.view.ConsoleView;

public class ConsoleController {

    private ConsoleView view;
    private Model model;

    public ConsoleController (ConsoleView view, Model model) {
        this.view = view;
        this.model = model;
    }

    public void run () {
        while (true) {
            String[] inputElements = view.getInput().split(" ");
            EducationalInstitution institution;
            String type = inputElements[0];
            String name = inputElements[1];
            int count = Integer.parseInt(inputElements[2]);
            switch (type){
                case "University":{
                    institution = new University(name, count);
                    break;
                }
                case "College":{
                    institution = new College(name, count);
                    break;
                }
                default:
                    throw new IllegalArgumentException("Illegal selected institution");
            }
            model.setEducationalInstitution(institution);
        }
    }
}
