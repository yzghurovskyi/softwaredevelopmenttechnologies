package com.gmail.kp61group4.task2.controller;

import com.gmail.kp61group4.task2.concreteEducationalInstitutions.College;
import com.gmail.kp61group4.task2.concreteEducationalInstitutions.University;
import com.gmail.kp61group4.task2.educationalInstitution.EducationalInstitution;
import com.gmail.kp61group4.task2.model.Model;
import com.gmail.kp61group4.task2.view.View;

import java.awt.event.ActionListener;

public class Controller  {
    private View view;
    private Model model;

    public Controller (View view, Model model) {
        this.view = view;
        this.model = model;

        view.getShowButton().addActionListener((actionEvent) -> {
            EducationalInstitution institution;
            String insName = view.getNameField().getText();
            int insCount = (int)view.getCount().getModel().getValue();
            switch (view.getButtonGroup().getSelection().getActionCommand()){
                case "University":{
                    institution = new University(insName, insCount);
                    break;
                }
                case "College":{
                    institution = new College(insName, insCount);
                    break;
                }
                default:
                    throw new IllegalArgumentException("Illegal selected institution");
            }
            model.setEducationalInstitution(institution);
        });
    }
}