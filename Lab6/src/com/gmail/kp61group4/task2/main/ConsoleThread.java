package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.controller.ConsoleController;
import com.gmail.kp61group4.task2.model.Model;
import com.gmail.kp61group4.task2.view.ConsoleView;

public class ConsoleThread  implements Runnable {
    private Model model;

    public ConsoleThread(Model model) {
        this.model = model;
    }

    @Override
    public void run() {
        ConsoleView view = new ConsoleView(model);
        ConsoleController controller = new ConsoleController(view, model);
        controller.run();
    }
}
