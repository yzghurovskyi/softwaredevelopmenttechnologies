package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.controller.Controller;
import com.gmail.kp61group4.task2.model.Model;
import com.gmail.kp61group4.task2.view.View;

public class ViewThread implements Runnable {
    private Model model;

    public ViewThread(Model model) {
        this.model = model;
    }

    @Override
    public void run() {
        View view = new View("Hello", model);
        Controller controller = new Controller(view, model);
        view.setVisible(true);
    }
}
