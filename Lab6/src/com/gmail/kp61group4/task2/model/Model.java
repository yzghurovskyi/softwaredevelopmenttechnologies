package com.gmail.kp61group4.task2.model;
import com.gmail.kp61group4.task2.educationalInstitution.EducationalInstitution;

import java.util.Observable;

public class Model extends Observable {
    private EducationalInstitution educationalInstitution;

    public void setEducationalInstitution(EducationalInstitution educationalInstitution) {
        this.educationalInstitution = educationalInstitution;
        setChanged();
        notifyObservers();

    }

    public EducationalInstitution getEducationalInstitution(){ return educationalInstitution; }

}
