package com.gmail.kp61group4.task2.view;

import com.gmail.kp61group4.task2.model.Model;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class ConsoleView implements Observer {
    private Model model;
    private Scanner scanner;

    public ConsoleView(Model model) {
        this.model = model;
        this.model.addObserver(this);
        this.scanner = new Scanner(System.in);
    }

    public String getInput() {
        System.out.println("Enter parameters for educational institution in format " +
                "\"[<College or University>:String <Name>:String, <Number of students>:Int]\": ");
        return scanner.nextLine();
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(model.getEducationalInstitution().info());
    }
}
