package com.gmail.kp61group4.task2.view;

import com.gmail.kp61group4.task2.model.Model;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class View extends JFrame implements Observer {
    private JTextField name = new JTextField(50);
    private SpinnerModel countModel = new SpinnerNumberModel(50, 0, 500000, 1);
    private JSpinner count = new JSpinner(countModel);

    private JRadioButton universityButton = new JRadioButton("University");
    private JRadioButton collegeButton = new JRadioButton("College");

    private JButton showButton = new JButton("Show info");
    private JTextArea infoArea = new JTextArea();

    private ButtonGroup buttonGroup = new ButtonGroup();

    private Model model;

    public JButton getShowButton() {
        return showButton;
    }

    public ButtonGroup getButtonGroup() {
        return buttonGroup;
    }

    public JTextField getNameField() {
        return name;
    }

    public JSpinner getCount() {
        return count;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public View (String title, Model model) {
        super("Educational");
        this.model = model;
        this.model.addObserver(this);

        this.setSize(650,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new GridLayout(3, 1));

        JPanel buttonPanel = new JPanel(new BorderLayout());
        buttonPanel.add(universityButton, BorderLayout.CENTER);
        buttonPanel.add(collegeButton, BorderLayout.WEST);

        buttonGroup.add(universityButton);
        buttonGroup.add(collegeButton);
        universityButton.setActionCommand("University");
        collegeButton.setActionCommand("College");
        universityButton.setSelected(true);

        JPanel dataPanel = new JPanel();
        dataPanel.setLayout(new FlowLayout());

        JLabel label = new JLabel("Enter name: ");
        JPanel textFieldPanel = new JPanel(new BorderLayout());
        textFieldPanel.add(label, BorderLayout.WEST);
        textFieldPanel.add(name, BorderLayout.CENTER);
        dataPanel.add(textFieldPanel, BorderLayout.PAGE_START);

        dataPanel.add(count);
        dataPanel.add(showButton);
        add(buttonPanel);
        add(dataPanel);
        add(infoArea);
        infoArea.setEditable(false);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(infoArea != null){
            infoArea.setText(model.getEducationalInstitution().info() + "\n");
        }
    }
}
