package com.gmail.kp61group4.task1.controller;

import com.gmail.kp61group4.task1.model.NumberStorage;
import com.gmail.kp61group4.task1.numberProcessingStrategies.*;
import com.gmail.kp61group4.task1.view.ConsoleView;

public class ConsoleController {
    private ConsoleView view;
    private NumberStorage numberStorage;

    public ConsoleController (ConsoleView view, NumberStorage numberStorage) {
        this.view = view;
        this.numberStorage = numberStorage;
    }

    public void run () {
        while (true) {
            int choose = view.getInput();
            NumberProcessingStrategy processingStrategy;
            switch (choose){
                case 1: {
                    processingStrategy
                            = new TextFilesNumberProcessingStrategy("input.txt", "output.txt");
                    break;
                }
                case 2: {
                    processingStrategy
                            = new BinaryFilesNumberProcessingStrategy("input.dat", "output.dat");
                    break;
                }
                case 3: {
                    processingStrategy
                            = new BinaryFileNumberProcessingStrategy("numbers.dat");
                    break;
                }
                case 4: {
                    processingStrategy
                            = new TextToBinaryProcessingStrategy("input.txt", "output.dat");
                    break;
                }
                case 5: {
                    processingStrategy
                            = new BinaryToTextProcessingStrategy("input.dat", "output.txt");
                    break;
                }
                default: {
                    view.printNotification("Please, choose between 1 and 5!");
                    continue;
                }
            }
            numberStorage.setNumberProcessingStrategy(processingStrategy);
            try {
                numberStorage.processNumbers();
            } catch (Exception ex){
                view.printNotification(ex.getMessage());
            }
        }
    }
}
