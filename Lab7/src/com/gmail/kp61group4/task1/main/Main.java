package com.gmail.kp61group4.task1.main;
import com.gmail.kp61group4.task1.controller.ConsoleController;
import com.gmail.kp61group4.task1.model.NumberStorage;
import com.gmail.kp61group4.task1.view.ConsoleView;

public class Main {
    public static void main(String[] args) {
        NumberStorage numberStorage = new NumberStorage();
        ConsoleView view = new ConsoleView(numberStorage);
        ConsoleController controller = new ConsoleController(view, numberStorage);
        controller.run();
    }
}
