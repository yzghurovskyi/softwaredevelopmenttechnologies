package com.gmail.kp61group4.task1.model;

import com.gmail.kp61group4.task1.numberProcessingStrategies.NumberProcessingStrategy;
import java.util.Observable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NumberStorage extends Observable {
    private static final int numbersCount = 100;
    private static final int minValue = -50;
    private static final int maxValue = 50;

    private List<Float> numbers;
    private NumberProcessingStrategy numberProcessingStrategy;

    public NumberStorage(List<Float> numbers){
        this.numbers = new ArrayList<>(numbers);
    }

    public NumberStorage(){
        fillByRandom();
    }

    public void setNumberProcessingStrategy(NumberProcessingStrategy numberProcessingStrategy) {
        this.numberProcessingStrategy = numberProcessingStrategy;
    }

    private void fillByRandom(){
        numbers = new ArrayList<>(numbersCount);
        Random rand = new Random();
        for(int i = 0; i < numbersCount; i++){
            numbers.add(minValue + rand.nextFloat() * (maxValue - minValue));
        }
    }

    public List<Float> getNumbers() {
        return new ArrayList<>(numbers);
    }

    public void processNumbers() throws Exception{
        prepareInputFile();
        numberProcessingStrategy.processNumbers();
        this.setChanged();
        this.notifyObservers();
    }

    private void prepareInputFile() throws IOException {
        fillByRandom();
        numberProcessingStrategy.prepareInputFile(this);
    }

    public List<Float> getResultNumbers() throws Exception{
        int negativeIndex = findLastNegativeIndex();
        if(negativeIndex == -1){
            throw new Exception("All numbers are positive!");
        }
        List<Float> resultNumbers = new ArrayList<>();
        float halfMin = numbers.get(negativeIndex) / 2.0f;
        for(float number : numbers){
            resultNumbers.add(number + halfMin);
        }
        return resultNumbers;
    }

    private int findLastNegativeIndex(){
        for(int i = numbers.size() - 1; i >= 0; i--){
            if(numbers.get(i) < 0){
                return i;
            }
        }
        return -1;
    }
}
