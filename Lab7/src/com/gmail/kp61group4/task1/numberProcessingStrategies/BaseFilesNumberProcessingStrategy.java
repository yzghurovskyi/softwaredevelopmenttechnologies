package com.gmail.kp61group4.task1.numberProcessingStrategies;

import com.gmail.kp61group4.task1.model.NumberStorage;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseFilesNumberProcessingStrategy implements NumberProcessingStrategy {

    public abstract void prepareInputFile(NumberStorage numberStorage) throws IOException;
    public abstract List<Float> read() throws IOException;
    public abstract void write(List<Float> numbers) throws IOException;

    protected void prepareTextInputFile(NumberStorage numberStorage, String fileName) throws IOException{
        FileWriter fileWriter = new FileWriter(fileName);
        for(float number: numberStorage.getNumbers())
            fileWriter.write(number + "\n");
        fileWriter.close();
    }

    protected void prepareBinaryInputFile(NumberStorage numberStorage, String fileName) throws IOException{
        DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(fileName));
        for(float number : numberStorage.getNumbers()){
            outputStream.writeFloat(number);
            outputStream.writeChar('\n');
        }
        outputStream.close();
    }

    protected List<Float> readBinary(String inputFileName) throws IOException {
        List<Float> numbers = new ArrayList<>();
        DataInputStream inputStream = new DataInputStream(new FileInputStream(inputFileName));
        while (inputStream.available() > 0) {
            numbers.add(inputStream.readFloat());
            if (inputStream.readChar() != '\n')
                break;
        }
        inputStream.close();
        return numbers;
    }

    protected List<Float> readText(String inputFileName) throws IOException {
        List<Float> numbers = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFileName));
        String result;
        while((result = bufferedReader.readLine()) != null){
            numbers.add(Float.parseFloat(result));
        }
        bufferedReader.close();
        return numbers;
    }

    protected void writeBinary(String outputFileName, List<Float> numbers) throws IOException {
        DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(outputFileName));
        for(float number : numbers){
            outputStream.writeFloat(number);
            outputStream.writeChar('\n');
        }
        outputStream.close();
    }

    protected void writeText(String outputFileName, List<Float> numbers) throws IOException {
        FileWriter fileWriter = new FileWriter(outputFileName);
        for(float number: numbers)
            fileWriter.write(number + "\n");
        fileWriter.close();
    }

    @Override
    public void processNumbers() throws Exception {
        List<Float> numbers = read();
        NumberStorage readNumbers = new NumberStorage(numbers);
        write(readNumbers.getResultNumbers());
    }
}
