package com.gmail.kp61group4.task1.numberProcessingStrategies;

import com.gmail.kp61group4.task1.model.NumberStorage;

import java.io.IOException;
import java.util.List;

public class BinaryFileNumberProcessingStrategy extends BaseFilesNumberProcessingStrategy {
    private String filename;

    public BinaryFileNumberProcessingStrategy(String filename){
        this.filename = filename;
    }

    @Override
    public void prepareInputFile(NumberStorage numberStorage) throws IOException {
        super.prepareBinaryInputFile(numberStorage, filename);
    }

    @Override
    public List<Float> read() throws IOException {
        return super.readBinary(filename);
    }

    @Override
    public void write(List<Float> numbers) throws IOException {
        super.writeBinary(filename, numbers);
    }
}
