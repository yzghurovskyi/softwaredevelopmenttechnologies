package com.gmail.kp61group4.task1.numberProcessingStrategies;

import com.gmail.kp61group4.task1.model.NumberStorage;

import java.io.IOException;
import java.util.List;

public class BinaryToTextProcessingStrategy extends BaseFilesNumberProcessingStrategy {
    private String inputFileName;
    private String outputFileName;

    public BinaryToTextProcessingStrategy(String inputFileName, String outputFileName){
        this.inputFileName = inputFileName;
        this.outputFileName = outputFileName;
    }
    @Override
    public void prepareInputFile(NumberStorage numberStorage) throws IOException {
        super.prepareBinaryInputFile(numberStorage, inputFileName);
    }

    @Override
    public List<Float> read() throws IOException {
        return super.readBinary(inputFileName);
    }

    @Override
    public void write(List<Float> numbers) throws IOException {
        super.writeText(outputFileName, numbers);
    }
}
