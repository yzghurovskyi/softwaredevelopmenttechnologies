package com.gmail.kp61group4.task1.numberProcessingStrategies;

import com.gmail.kp61group4.task1.model.NumberStorage;

import java.io.IOException;
import java.util.List;

public interface NumberProcessingStrategy {
    void prepareInputFile(NumberStorage numberStorage) throws IOException;
    List<Float> read() throws IOException;
    void write(List<Float> numbers) throws IOException;
    void processNumbers() throws Exception;
}
