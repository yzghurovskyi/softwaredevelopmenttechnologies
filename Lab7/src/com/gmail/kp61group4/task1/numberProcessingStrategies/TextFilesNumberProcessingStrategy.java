package com.gmail.kp61group4.task1.numberProcessingStrategies;

import com.gmail.kp61group4.task1.model.NumberStorage;

import java.io.*;
import java.util.List;

public class TextFilesNumberProcessingStrategy extends BaseFilesNumberProcessingStrategy {
    private String inputFileName;
    private String outputFileName;

    public TextFilesNumberProcessingStrategy(String inputFileName, String outputFileName){
        this.inputFileName = inputFileName;
        this.outputFileName = outputFileName;
    }

    @Override
    public void prepareInputFile(NumberStorage numberStorage) throws IOException {
        super.prepareTextInputFile(numberStorage, inputFileName);
    }

    public List<Float> read() throws IOException{
        return super.readText(inputFileName);
    }

    public void write(List<Float> numbers) throws IOException{
        super.writeText(outputFileName, numbers);
    }
}
