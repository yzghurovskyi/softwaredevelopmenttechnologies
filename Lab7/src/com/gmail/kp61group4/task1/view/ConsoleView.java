package com.gmail.kp61group4.task1.view;

import com.gmail.kp61group4.task1.model.NumberStorage;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class ConsoleView implements Observer {
    private NumberStorage numberStorage;
    private Scanner scanner;

    public ConsoleView(NumberStorage numberStorage) {
        this.numberStorage = numberStorage;
        this.numberStorage.addObserver(this);
        this.scanner = new Scanner(System.in);
    }

    public int getInput() {
        System.out.println("Choose a method for processing numbers: \n" +
                "1. Text files\n"+
                "2. Binary files\n"+
                "3. One binary file\n"+
                "4. From text to binary\n"+
                "5. From binary to text\n");
        return scanner.nextInt();
    }

    public void printNotification(String notification){
        System.out.println(notification);
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            System.out.println("Input: " + numberStorage.getNumbers());
            System.out.println("Result: " + numberStorage.getResultNumbers());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
