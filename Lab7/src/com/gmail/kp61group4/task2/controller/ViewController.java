package com.gmail.kp61group4.task2.controller;

import com.gmail.kp61group4.task2.model.Model;
import com.gmail.kp61group4.task2.view.View;

public class ViewController {
    private View view;
    private Model model;

    public ViewController (View view, Model model) {
        this.view = view;
        this.model = model;

        view.getButton().addActionListener((actionEvent) ->
                model.setText(view.getTextField().getText()));
    }
}
