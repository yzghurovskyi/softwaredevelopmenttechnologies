package com.gmail.kp61group4.task2.fileProcess;

import com.gmail.kp61group4.task2.fileProcess.FileProcess;

import java.io.FileOutputStream;

public class FileWrite implements FileProcess {
    private String path;
    private String textToWrite;
    private FileOutputStream outputStream;

    public FileWrite(String path, String textToWrite) {
        this.path = path;
        this.textToWrite = textToWrite;
    }

    @Override
    public void process() {
        try {
            outputStream = new FileOutputStream(path);
            outputStream.write(textToWrite.getBytes("UTF-8"));
            outputStream.close();
        } catch (Exception f) {
            f.printStackTrace();
        }
    }
}
