package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.controller.ViewController;
import com.gmail.kp61group4.task2.model.Model;
import com.gmail.kp61group4.task2.view.View;


public class Main {
    public static void main(String[] args) {
        Model model = new Model();
        View view = new View("Hello", model);
        ViewController controller = new ViewController(view, model);
        view.setVisible(true);

    }
}
