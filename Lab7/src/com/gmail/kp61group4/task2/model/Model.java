package com.gmail.kp61group4.task2.model;

import com.gmail.kp61group4.task2.sentance.Sentence;
import com.gmail.kp61group4.task2.text.Text;

import java.util.Observable;

public class Model extends Observable {
    private Text text;
    public Model(){
        text = new Text();
    }

    public void setText(String sentance) {
        Sentence sentence = new Sentence(sentance);
        text.add(sentence);
        text.textProcess();
        setChanged();
        notifyObservers();
    }

    public Text getText() {
        return text;
    }

}
