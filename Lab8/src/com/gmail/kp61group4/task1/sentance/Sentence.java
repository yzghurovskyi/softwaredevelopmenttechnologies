package com.gmail.kp61group4.task1.sentance;

import java.util.HashMap;
import java.util.Objects;

public class Sentence {
    private String sentence;

    public Sentence() {
        sentence = "";
    }

    public Sentence(String sentence) {
        this.sentence = sentence;
    }


    public HashMap<String, String> processSentance() {
        HashMap<String, String> resultMap = new HashMap<String, String>();
        String[] words;
        words = sentence.split(" ");

        for(int i = 0; i < words.length-1; i++) {
            char[] curWord = words[i].toCharArray();
            char[] nextWord = words[i+1].toCharArray();
            if(!words[i].equals("") && !words[i+1].equals("") &&
                    curWord[words[i].length()-1] == nextWord[0]) {
                resultMap.put(words[i], words[i+1]);
            }
        }
        return resultMap;
    }


    @Override
    public String toString() {
        return "Sentence='" + sentence + "\n";
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence1 = (Sentence) o;
        return Objects.equals(sentence, sentence1.sentence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence);
    }
}
