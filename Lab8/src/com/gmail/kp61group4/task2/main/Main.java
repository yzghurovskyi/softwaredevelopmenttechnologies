package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.concreteEducationalInstitutions.College;
import com.gmail.kp61group4.task2.concreteEducationalInstitutions.University;
import com.gmail.kp61group4.task2.educationalInstitution.EducationalInstitution;
import com.gmail.kp61group4.task2.serializer.Serializer;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<EducationalInstitution> institutions = new ArrayList<>() {{
            add(new College("MAUP", 84));
            add(new University("KNU", 23000));
            add(new University("KPI", 34000));
        }};
        Serializer serializer = new Serializer("institutions.dat");

        System.out.println("Serialize");
        //serializer.serializeInstitutions(institutions);

        for(var institution : institutions){
            institution.printInfo();
        }

        List<EducationalInstitution> deserializedInstitutions = serializer.deserializeInstitutions(institutions.size());
        System.out.println("\nDeserialize:");

        for(var institution : deserializedInstitutions){
            institution.printInfo();
        }
    }
}
