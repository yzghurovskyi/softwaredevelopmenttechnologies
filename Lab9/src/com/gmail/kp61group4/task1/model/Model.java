package com.gmail.kp61group4.task1.model;


import com.gmail.kp61group4.task1.numberProcess.NumberProcess;

import java.util.Observable;

public class Model extends Observable {
    private NumberProcess numberProcess;
    public Model(){
        numberProcess = new NumberProcess();
        numberProcess.fillArrayWithRandomValues();
    }

    public void setArray(String array) {
        String[] numbers = array.split(",");
        int[] numbersArray = new int[numbers.length];
        for(int i = 0; i < numbers.length; i++) {
            numbersArray[i] = Integer.parseInt(numbers[i]);
        }
        numberProcess.setNumbers(numbersArray);
        setChanged();
        notifyObservers();
    }

    public NumberProcess getNumberProcess() {
        return numberProcess;
    }

}
