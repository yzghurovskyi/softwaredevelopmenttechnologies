package com.gmail.kp61group4.task1.numberProcess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class NumberProcess {
    private int[] numbers;
    private int maxRandom = 50;
    private int minRandom = -50;
    private int randomArrayLength = 5;

    public NumberProcess() {
        numbers = new int[randomArrayLength];
    }

    public List<Integer> multiplyToMinNumber(){
        List<Integer> result = new ArrayList<>();
        int minValue = findMin();
        System.out.println(minValue);
        for(int val: numbers) {
            System.out.println(val * minValue);
            result.add(val * minValue);
        }
        return result;
    }

    public void fillArrayWithRandomValues() {
        int[] numbers = new int[randomArrayLength];
        Random rand = new Random();
        for (int i = 0; i < randomArrayLength; i++) {
            numbers[i] = rand.nextInt(maxRandom - minRandom + 1) + minRandom;
        }
        System.arraycopy(numbers, 0, this.numbers, 0, numbers.length);
    }

    public void setNumbers(int[] numbers) {
        this.numbers = new int[numbers.length];
        System.arraycopy(numbers, 0, this.numbers, 0, numbers.length);
    }

    public int findMin(){
        int minValue = numbers[0];
        for(int i=1;i<numbers.length;i++){
            if(numbers[i] < minValue){
                minValue = numbers[i];
            }
        }
        return minValue;
    }

    public double getElementByIndex(int index){
        try {
            return numbers[index];
        } catch(IndexOutOfBoundsException ex) {
            System.out.println("Invalid index!!!\r\nTry again!");
            return Double.NaN;
        }
    }

    public int getStorageLength(){
        return numbers.length;
    }

    public void setValueByIndex(int value, int index){
        try {
            numbers[index] = value;
        } catch(IndexOutOfBoundsException ex){
            System.out.println("Invalid index!!!\r\nTry again!");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumberProcess that = (NumberProcess) o;
        return Arrays.equals(numbers, that.numbers);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(numbers);
    }

    @Override
    public String toString() {
        return "NumberProcess{" +
                "numbers=" + Arrays.toString(numbers) +
                '}';
    }
}
