package com.gmail.kp61group4.task1.view;

import com.gmail.kp61group4.task1.model.Model;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class View extends JFrame implements Observer {
    private JButton button;
    private JTextField textField;
    private JTextArea textArea;
    private JMenuItem menuOpen;
    private JMenuItem menuSave;
    private Model model;

    public View (String title, Model model) {
        super(title);
        this.model = model;
        this.model.addObserver(this);
        setSize( 400, 500 );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );


        JPanel north = new JPanel();
        north.setLayout(new GridLayout(3, 2));
        textField = new JTextField();
        JLabel label = new JLabel("Enter text: ");
        JPanel textFieldPanel = new JPanel(new BorderLayout());
        textFieldPanel.add(label, BorderLayout.WEST);
        textFieldPanel.add(textField, BorderLayout.CENTER);

        north.add(textFieldPanel, BorderLayout.NORTH);
        add(north, BorderLayout.PAGE_START);


        button = new JButton("Process numbers");
        textArea = new JTextArea();
        textArea.setEditable(false);
        add(textArea);
        JPanel panel = new JPanel();
        panel.add(button);
        add(panel, BorderLayout.PAGE_END);
        fillTextField();

    }

    public JButton getButton() {
        return button;
    }


    public JTextField getTextField() {
        return textField;
    }

    @Override
    public void update(Observable o, Object arg) {
        fillTextField();
    }

    public void fillTextField(){
        if (textArea != null) {
            textField.setText("");
            String resultString = "Array: ";
            for(int i = 0; i < model.getNumberProcess().getStorageLength();i++ ) {
                resultString += model.getNumberProcess().getElementByIndex(i) + " ";
            }
            resultString += "\n";
            ArrayList<Integer> numbers = (ArrayList) model.getNumberProcess().multiplyToMinNumber();
            resultString += "Result: ";
            for (Integer value : numbers) {
                resultString += value + " ";
            }
            textArea.setText(resultString);
        }
    }
}
