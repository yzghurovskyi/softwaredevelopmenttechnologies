package com.gmail.kp61group4.task2.card;

import java.util.Date;
import java.util.Objects;

public class PersonCard {
    private String lastName;
    private Date birthday;

    public PersonCard(String lastName, Date birthday){
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    @Override
    public String toString() {
        return "PersonCard{" +
                "lastName='" + lastName + '\'' +
                ", birthday=" + birthday.toLocaleString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonCard)) return false;
        PersonCard that = (PersonCard) o;
        return lastName.equals(that.lastName) &&
                birthday.equals(that.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, birthday);
    }
}
