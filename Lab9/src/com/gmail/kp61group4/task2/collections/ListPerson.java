package com.gmail.kp61group4.task2.collections;

import com.gmail.kp61group4.task2.card.PersonCard;
import com.gmail.kp61group4.task2.zodiak.Zodiak;

import java.util.Calendar;
import java.util.HashMap;

public class ListPerson {
    private HashMap<String, PersonCard> cards = new HashMap<>();

    public ListPerson(){ }

    public ListPerson(HashMap<String, PersonCard> cards){
        this.cards = new HashMap<>(cards);
    }

    public PersonCard add(PersonCard card){
        if(card == null){
            throw new IllegalArgumentException("Person card can not be null");
        }
        return cards.put(card.getLastName(), card);
    }

    public PersonCard delete(String lastName){
        return cards.remove(lastName);
    }

    public PersonCard get(String lastName){
        return cards.getOrDefault(lastName, null);
    }

    public ListPerson union(ListPerson list){
        cards.putAll(list.cards);

        return new ListPerson(cards);
    }

    public ListPerson intersection(ListPerson list){
        ListPerson listPerson = new ListPerson(cards);
        listPerson.cards.keySet().retainAll(list.cards.keySet());
        return listPerson;
    }

    public ListPerson difference(ListPerson list){
        ListPerson listPerson = new ListPerson(cards);
        listPerson.cards.entrySet().removeAll(list.cards.entrySet());
        return listPerson;
    }

    public Zodiak determineZodiak(String lastName){
        PersonCard card = get(lastName);
        if(card == null){
            throw new IllegalArgumentException("Can not find Person with Last name" + lastName);
        }
        Calendar birthday = Calendar.getInstance();
        birthday.setTime(card.getBirthday());

        for(Zodiak zodiak : Zodiak.values()){
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            start.setTime(zodiak.getStart());
            end.setTime(zodiak.getEnd());

            if(birthday.get(Calendar.MONTH) >= start.get(Calendar.MONTH) &&
                    birthday.get(Calendar.MONTH) <= end.get(Calendar.MONTH) &&
                    (birthday.get(Calendar.DAY_OF_MONTH) >= start.get(Calendar.DAY_OF_MONTH) ||
                    birthday.get(Calendar.DAY_OF_MONTH) <= end.get(Calendar.DAY_OF_MONTH))){
                return zodiak;
            }
        }
        throw new IllegalArgumentException("Invalid birthday date");
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (PersonCard card: cards.values()){
            stringBuilder.append(card.toString());
        }
        return stringBuilder.toString();
    }
}
