package com.gmail.kp61group4.task2.main;

import com.gmail.kp61group4.task2.card.PersonCard;
import com.gmail.kp61group4.task2.collections.ListPerson;

import java.util.Date;

public class Main {
    public static void main(String[] args){
        PersonCard card1 = new PersonCard("Vynnyk", new Date(1976, 4, 5));
        PersonCard card2 = new PersonCard("Golovach", new Date(1965, 1, 3));
        PersonCard card3 = new PersonCard("Shcherbyna", new Date(1999, 3, 23));

        ListPerson list1 = new ListPerson(){{
            add(card1); add(card2); add(card3);
        }};

        ListPerson list2 = new ListPerson(){{
            add(card1);
        }};

        System.out.println(list1.intersection(list2));
        System.out.println(list1.union(list2));
        System.out.println(list1.difference(list2));
        System.out.println(list1.determineZodiak(card1.getLastName()));
    }
}
