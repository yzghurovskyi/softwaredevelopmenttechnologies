package com.gmail.kp61group4.task2.zodiak;

import java.util.Calendar;
import java.util.Date;

public enum Zodiak {
    ARIES(new Date(0, Calendar.MARCH, 21), new Date(0, Calendar.APRIL, 19)),
    TAURUS(new Date(0, Calendar.APRIL, 20), new Date(0, Calendar.MAY, 20)),
    GEMINI(new Date(0, Calendar.MAY, 21), new Date(0, Calendar.JUNE, 20)),
    CANCER(new Date(0, Calendar.JUNE, 21), new Date(0, Calendar.JULY, 22)),
    LEO(new Date(0, Calendar.JULY, 23), new Date(0, Calendar.AUGUST, 22)),
    VIRGO(new Date(0, Calendar.AUGUST, 23), new Date(0, Calendar.SEPTEMBER, 22)),
    LIBRA(new Date(0, Calendar.SEPTEMBER, 23), new Date(0, Calendar.OCTOBER, 22)),
    SCORPIO(new Date(0, Calendar.OCTOBER, 23), new Date(0, Calendar.NOVEMBER, 21)),
    SAGITTARIUS(new Date(0, Calendar.NOVEMBER, 22), new Date(0, Calendar.DECEMBER, 21)),
    CAPRICORN(new Date(0, Calendar.DECEMBER, 22), new Date(0, Calendar.JANUARY, 19)),
    AQUARIUS(new Date(0, Calendar.JANUARY, 20), new Date(0, Calendar.FEBRUARY, 18)),
    PICSEC(new Date(0, Calendar.FEBRUARY, 19), new Date(0, Calendar.MARCH, 20));


    private Date start;
    private Date end;

    Zodiak(Date start, Date end){
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "Zodiak{" +
                "name='" + this.name() + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
