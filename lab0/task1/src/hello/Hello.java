package hello;

public class Hello {
    public static void main(String[] args) {
        System.out.println(sayHello("Vadim"));
    }

    public static String sayHello(String name){
        return "Hello, " + name;
    }
}
