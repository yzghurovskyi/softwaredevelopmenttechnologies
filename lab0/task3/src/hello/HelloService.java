package hello;

import javax.swing.*;

public class HelloService {
    public String askName(){
        return JOptionPane.showInputDialog("What is your name?");
    }

    public void sayHello(String name){
        JOptionPane.showMessageDialog( null, "Hello, " + name );
    } 
}
