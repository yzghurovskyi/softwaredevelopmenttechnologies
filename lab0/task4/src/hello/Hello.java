package hello;

public class Hello {
    public static void main(String[] args) {
		HelloService helloService = new HelloService();
		String name = helloService.askName();
		helloService.sayHello(name);
		System.exit(0);
    }
}
